﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace Business
{
    public class ExportHelp
    {
        protected LetterInfoEntities db = new LetterInfoEntities();
        public static string testExcel(DataSet ds,string path,bool isAll = false)
        {
            var workbook = new HSSFWorkbook();
            List<ISheet> sheets = new List<ISheet>();
            if (isAll)
            {
                ISheet sheet0 = workbook.CreateSheet("所有案件信息");
                sheets = new List<ISheet>
                {
                   sheet0
                };
            }
            else
            {
                ISheet sheet0 = workbook.CreateSheet("案件管理");
                ISheet sheet1 = workbook.CreateSheet("基本信息");
                ISheet sheet2 = workbook.CreateSheet("党政领导接访信息");
                ISheet sheet3 = workbook.CreateSheet("领导约下访");
                ISheet sheet4 = workbook.CreateSheet("案件包案情况");
                ISheet sheet5 = workbook.CreateSheet("信访办理信息");
                ISheet sheet6 = workbook.CreateSheet("进京赴省情况");
                 sheets = new List<ISheet>
                {
                    sheet0,sheet1,sheet2,sheet3,sheet4,sheet5,sheet6
                };
            }
  

            var newBook = BuildWorkbook(ds.Tables[0],ds, workbook, sheets);

            //打开xls文件，如没有则创建，如存在则在创建是不要打开该文件
            using (var fs = File.OpenWrite(path))
            {
                newBook.Write(fs);   
                return "成功";
            }
      
        }

        /// <summary>
        ///  组装workbook.
        /// </summary>
        /// <param name="dt">dataTable资源</param>
        /// <param name="columnHeader">表头</param>
        /// <returns></returns>
        public static HSSFWorkbook BuildWorkbook(DataTable dt, DataSet ds, HSSFWorkbook workbook, List<ISheet> sheets, string columnHeader = "")
        {
            //var workbook = new HSSFWorkbook();
            //ISheet sheet = workbook.CreateSheet(string.IsNullOrWhiteSpace(dt.TableName) ? "Sheet1" : dt.TableName);
  
       

            #region 文件属性信息
            //{
            //    var dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            //    dsi.Company = "NPOI";
            //    workbook.DocumentSummaryInformation = dsi;

            //    SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            //    si.Author = "文件作者信息";
            //    si.ApplicationName = "创建程序信息";
            //    si.LastAuthor = "最后保存者信息";
            //    si.Comments = "作者信息";
            //    si.Title = "标题信息";
            //    si.Subject = "主题信息";
            //    si.CreateDateTime = DateTime.Now;
            //    workbook.SummaryInformation = si;
            //}
            #endregion

            var dateStyle = workbook.CreateCellStyle();
            var format = workbook.CreateDataFormat();
            dateStyle.DataFormat = format.GetFormat("yyyy-mm-dd");
          
            var index = 0;
            foreach (var item in sheets)
            {
                var len = ds.Tables.Count;
              //  var len1 = ds.Tables[index].Rows.Count;
                var arrColWidth = new int[ds.Tables[index].Columns.Count];
                foreach (DataColumn itemColumn in ds.Tables[index].Columns)
                {
                    arrColWidth[itemColumn.Ordinal] = Encoding.GetEncoding(936).GetBytes(itemColumn.ColumnName.ToString()).Length;
                }
                for (var s = 0; s < ds.Tables[index].Rows.Count; s++)
                {
                    for (var j = 0; j < ds.Tables[index].Columns.Count; j++)
                    {
                        int intTemp = Encoding.GetEncoding(936).GetBytes(ds.Tables[index].Rows[s][j].ToString()).Length;
                        if (intTemp > arrColWidth[j])
                        {
                            arrColWidth[j] = intTemp;
                        }
                    }
                }

                #region 列头及样式
                {
                    var headerRow = item.CreateRow(0);
                    //CellStyle
                    headerRow.HeightInPoints = 20; //设置列头行高
                    ICellStyle headStyle = workbook.CreateCellStyle();
                    headStyle.Alignment = HorizontalAlignment.Left;// 左右居中    
                    headStyle.VerticalAlignment = VerticalAlignment.Center;// 上下居中 
                    //headStyle.WrapText = true;//自动换行


                    //定义font
                    IFont font = workbook.CreateFont();
                    font.FontHeightInPoints = 10;
                    font.Boldweight = 700;
                    headStyle.SetFont(font);

                    foreach (DataColumn column in ds.Tables[index].Columns)
                    {
                        headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                        headerRow.GetCell(column.Ordinal).CellStyle = headStyle;
                       // var width = (arrColWidth[column.Ordinal] + 1) * 256;
                       // item.SetColumnWidth(column.Ordinal, width);   //列宽问题
                    }
         
                }
                #endregion

                #region 内容及样式
                for (int i = 0; i < ds.Tables[index].Rows.Count; i++)
                {
                    IRow drow = item.CreateRow(i + 1);
                    ICellStyle contentStyle = workbook.CreateCellStyle();
                    //drow.HeightInPoints = 20;
                    contentStyle.VerticalAlignment = VerticalAlignment.Top;// 上下居中 
                    contentStyle.WrapText = true;//自动换行
                    for (int j = 0; j < ds.Tables[index].Columns.Count; j++)
                    {
                        ICell cell = drow.CreateCell(j, CellType.String);
                        cell.CellStyle = contentStyle;//设置换行
                        cell.SetCellValue(ds.Tables[index].Rows[i][j].ToString());
                       
                    }
                }
                #endregion

                index++;
            }

            #region 自动列宽          
            for (int i = 0; i < sheets.Count; i++)
            {
                for (int j = 0; j <ds.Tables[i].Columns.Count; j++)
                {
                    var str = ds.Tables[i].Columns[j].ColumnName;
                    if (ds.Tables[i].Columns[j].ColumnName.Equals("信访内容"))
                    {
                        sheets[i].DefaultColumnWidth = 100;
                    }
                    else
                    {
                        sheets[i].AutoSizeColumn(j, true);
                    }
                }
            }
            #endregion

            return workbook;
        }


    }

}
