﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class LeaderMeetingController : BaseController
    {
        public ActionResult LeaderMeeting()
        {
            var selectLeader = db.Database.SqlQuery<LeaderMeetingModel>("select LeaderId,LeaderName+'('+LeaderPosition+')' as NamePosition,Status from t_Leader").Where(n => n.Status == 0).ToList();
            ViewData["selectLeader"] = new SelectList(selectLeader, "LeaderId", "NamePosition");

            var selectNumber = db.t_CaseInfo.Where(n => n.IsDelete == 0).ToList();
            ViewData["selectNumber"] = new SelectList(selectNumber, "ID", "AppealNumber");
            return View();
        }

        [HttpPost] //获取领导接访信息
        public ActionResult GetMeeting(int pageindex, int pagesize)
        {
            //CurrentUser.UserUnit
            string querySQL = "select s.LeaderId,LeaderName,LeaderPosition,count(distinct CaseInfoID) as Lcount,count(CaseInfoID) as Ncount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID where IsDelete = 0 Group by s.LeaderId,s.LeaderName,s.LeaderPosition";
            var temp = db.Database.SqlQuery<LeaderMeetingModel>(querySQL)
                .OrderByDescending(n => n.LeaderName)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderMeetingPartial", temp);
        }

        [HttpPost] //查询领导接访信息
        public ActionResult QueryMeeting(int pageindex, int pagesize,string leaderId,string petitionName)
        {

            if (string.IsNullOrEmpty(petitionName))
            {
                string querySQL = "select s.LeaderId,LeaderName,LeaderPosition,count(distinct CaseInfoID) as Lcount,count(CaseInfoID) as Ncount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID where IsDelete = 0 and s.LeaderId = " + leaderId + " Group by s.LeaderId,s.LeaderName,s.LeaderPosition";
                var queryList = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).ToList();

                ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).Count();

                return PartialView("LeaderMeetingPartial", queryList);
            }
            else
            {
                string expression = string.Empty;
                if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
                {
                    expression = " ";
                }
                else
                {
                    expression = " and c.RelateUnitID like '%" + CurrentUser.UserUnit + "%'";
                }

                if (!string.IsNullOrEmpty(leaderId))
                {
                    leaderId = " and l.LeaderID = " + leaderId;
                }

                List<LeaderMeetingModel> queryList = new List<LeaderMeetingModel>();

                string querySQL = "select PetitionName,* from[dbo].[t_LeaderMeeting] l inner join[dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID inner join [dbo].[t_Leader] le on l.LeaderID = le.LeaderId where c.IsDelete = 0 and PetitionName like '%"+petitionName+"%'" + leaderId + expression;
                queryList = db.Database.SqlQuery<LeaderMeetingModel>(querySQL)
                    .OrderByDescending(n => n.ID)
                    .Skip(pagesize * (pageindex - 1))
                    .Take(pagesize).ToList();

                ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).Count();

                ViewData["pageindex"] = pageindex;
                ViewData["pagesize"] = pagesize;

                int pageIndex = pageindex;//当前页
                int pageSize = pagesize;//显示条数

                return PartialView("SearchMeetingPartial", queryList);
            }
        }

        [HttpPost] //获取诉求详情
        public ActionResult GetContent(string appealNumber)
        {
            var selectNumber = db.t_CaseInfo.FirstOrDefault(n => n.AppealNumber == appealNumber);
            string title = selectNumber.AppealTitle;
            string content = selectNumber.AppealContent;
            string jsonStr = string.Empty;
            jsonStr += "{\"title\":"+"\""+title+"\""+",\"content\":" + "\"" + content + "\"" + "}";
            return Json(new MessageData(true, jsonStr));
        }

        [HttpPost] //获取案件详情
        public ActionResult GetCaseInfoList(int leaderId)
         {
            //string expression = Power();
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = " ";
            }
            else
            {
                expression = " and s.RelateUnitID like '%" + CurrentUser.UserUnit + "%'";
            }

            var caseInfoList = db.Database.SqlQuery<LeaderMeetingModel>(@"select distinct CaseInfoID,m.LeaderID,AppealNumber,
                
                AppealTitle,AppealContent,PetitionName,CountyName,UnitName,[RelateUnitID], [RelateAreaID],[RelateUnitName], [RelateAreaName] from [t_LeaderMeeting] m
                inner join t_CaseInfo s on s.ID = m.CaseInfoID left outer join t_County c on c.CountyId = s.TownshipType 
                left outer join t_Responsibility r on r.TableId = s.UnitType where m.IsDelete =0 and LeaderID = " + leaderId + expression).ToList(); //&& n.Status == 0  m.IsDelete = 0 and 
                
            return PartialView("SeeMeetingModal", caseInfoList);
        }

        [HttpPost] //获取领导详情
        public ActionResult GetLeaderDetails(int leaderId)
        {
            var leaderDetails = db.t_Leader.Where(n => n.LeaderId == leaderId).ToList(); //&& n.Status == 0

            return PartialView("SeeLeaderModal", leaderDetails);
        }

        public ActionResult MeetingList(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                var selectLeader = db.Database.SqlQuery<LeaderMeetingModel>("select LeaderId,LeaderName +' ' +'('+LeaderPosition+')' as NamePosition,Status from t_Leader").Where(n => n.Status == 0).ToList();
                ViewData["selectLeader"] = new SelectList(selectLeader, "LeaderId", "NamePosition");
                ViewData["ID"] = id;
                return View();
            }
        }

        [HttpPost] //获取接访记录信息
        public ActionResult GetMeetingList(int pageindex, int pagesize, int id)
         {
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = " ";
            }
            else
            {
                expression = " and c.RelateUnitID like '%" + CurrentUser.UserUnit + "%'";
            }
            
            string querySQL = "select * from[dbo].[t_LeaderMeeting] l inner join[dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID inner join [dbo].[t_Leader] le on l.LeaderID = le.LeaderId where l.IsDelete = 0 and l.LeaderID = " + id + expression;
            var temp = db.Database.SqlQuery<LeaderMeetingModel>(querySQL)
                .OrderByDescending(n => n.LeaderName)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();
            
            ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).Count();
            //ViewData["total"] = db.t_LeaderMeeting.Where(n => n.IsDelete == 0 && n.LeaderID == id).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;
            
            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("MeetingPartial", temp);
        }

        [HttpPost] //查询接访记录信息
        public ActionResult QueryMeetingList(int pageindex, int pagesize, int selectLeader,string startDate, string endDate, int id)
        {
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = " ";
            }
            else
            {
                expression = " and c.RelateUnitID like '%" + CurrentUser.UserUnit + "%'";
            }


            List<LeaderMeetingModel> queryList = new List<LeaderMeetingModel>();
            var dateStartDate = "1899-01-01";
            var dateEndtDate = "2099-12-31";
            if (id != 0)
            {
                expression += "and l.LeaderID = " + id;
            }
            if (startDate != "")
            {
                dateStartDate =startDate;
                expression += "and ReceptionDate >= '" + dateStartDate+"'";
            }
            if (endDate != "")
            {
                dateEndtDate = endDate;
                expression += "and ReceptionDate <= '" + dateEndtDate + "'";
            }
            string querySQL = "select * from[dbo].[t_LeaderMeeting] l inner join[dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID inner join [dbo].[t_Leader] le on l.LeaderID = le.LeaderId where l.IsDelete = 0 " + expression;
            queryList = db.Database.SqlQuery<LeaderMeetingModel>(querySQL)
                             .OrderByDescending(n => n.ID)
                             .Skip(pagesize * (pageindex - 1))
                             .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(querySQL).Count();
            
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("MeetingPartial", queryList);
        }

        [HttpPost] //添加接访记录
        public ActionResult AddMeetingList(LeaderMeetingModel model)
        {
            var caseInfoId1 = db.t_CaseInfo.Where(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ToList();
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            t_LeaderMeeting newMeeting = new t_LeaderMeeting();
            newMeeting.CaseInfoID = caseInfoId;
            newMeeting.LeaderID = model.LeaderID;
            newMeeting.ReceptionDate = model.ReceptionDate;
            newMeeting.ReceptionComment = model.ReceptionComment;
            newMeeting.ReceptionAddress = model.ReceptionAddress;
            newMeeting.ReceptionPerson = model.ReceptionPerson;
            db.t_LeaderMeeting.Add(newMeeting);
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        public ActionResult DelMeetingList(int id)
        {
            var newMeeting = db.t_LeaderMeeting.FirstOrDefault(n => n.ID == id);
            newMeeting.IsDelete = 1;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }

    }
}