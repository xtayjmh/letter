﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class BasicDataController : BaseController
    {
        #region 片区/乡镇管理
        public ActionResult RegionData()
        {
            return View();
        }

        [HttpPost] //获取片区/乡镇数据
        public ActionResult GetRegion(int pageindex, int pagesize)
        {
            var temp = db.t_County.Where(n=>n.Status == 0)
                            .OrderByDescending(n=>n.CountyId)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_County.Where(n=>n.Status==0).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("RegionPartial", temp);
        }

        [HttpPost] //添加片区/乡镇数据
        public ActionResult AddRegion(CountyModel model)
        {
            t_County newCounty = new t_County();
            newCounty.CountyName = model.CountyName.Trim();
            db.t_County.Add(newCounty);

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch(Exception)
            {
                return Json(new MessageData(false, "添加失败"));
            }

            
        }

        [HttpPost] //修改片区/乡镇数据
        public ActionResult EditRegion(CountyModel model)
        {
            var newCounty = db.t_County.FirstOrDefault(n => n.CountyId == model.ID);
            newCounty.CountyName = model.CountyName.Trim();

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }


        }

        [HttpPost] //删除片区/乡镇数据
        public ActionResult DelRegion(int id)
        {
            var queryCaseInfo = db.t_CaseInfo.Where(n => n.TownshipType == id && n.IsDelete == 0).ToList();
            if (queryCaseInfo.Count == 0)
            {
                var newCounty = db.t_County.FirstOrDefault(n => n.CountyId == id);
                newCounty.Status = 1;

                try
                {
                    db.SaveChanges();
                    return Json(new MessageData(true, "删除成功"));
                }
                catch (Exception)
                {
                    return Json(new MessageData(false, "删除失败"));
                }
            }
            else
            {
                return Json(new MessageData(false, "已有案件关联此稳控单位，无法删除"));
            }
            


        }
        #endregion

        #region 党政领导信息管理
        public ActionResult LeaderData()
        {
            return View();
        }

        [HttpPost] //获取领导信息
        public ActionResult GetLeader(int pageindex, int pagesize)
        {
            var temp = db.t_Leader.Where(n => n.Status == 0)
                            .OrderByDescending(n => n.LeaderId)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_Leader.Where(n => n.Status == 0).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderPartial", temp);
        }

        [HttpPost] //查询领导信息
        public ActionResult QueryLeader(int pageindex, int pagesize,string leaderName, string postion)
        {
            var queryList = db.t_Leader.Where(n => n.LeaderName.Contains(leaderName) && n.LeaderPosition.Contains(postion) && n.Status == 0)
                            .OrderByDescending(n => n.LeaderId)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_Leader.Where(n => n.LeaderName.Contains(leaderName) && n.LeaderPosition.Contains(postion) && n.Status == 0).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderPartial", queryList);
        }

        [HttpPost] //添加领导信息
        public ActionResult AddLeader(LeaderModel model)
        {
            t_Leader newLeader = new t_Leader();
            newLeader.LeaderName = model.LeaderName.Trim();
            newLeader.LeaderSex = model.LeaderSex;
            newLeader.LeaderPosition = model.LeaderPostion.Trim();
            newLeader.Remark = model.Remark;

            db.t_Leader.Add(newLeader);

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception e)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost] //修改领导信息
        public ActionResult EditLeader(LeaderModel model)
        {
            var newLeader = db.t_Leader.FirstOrDefault(n => n.LeaderId == model.ID);
            newLeader.LeaderName = model.LeaderName.Trim();
            newLeader.LeaderSex = model.LeaderSex.Trim();
            newLeader.LeaderPosition = model.LeaderPostion.Trim();
            newLeader.Remark = model.Remark;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost] //删除领导信息
        public ActionResult DelLeader(int id)
        {
            var newLeader = db.t_Leader.FirstOrDefault(n => n.LeaderId == id);
            newLeader.Status = 1;
            var queryLeader = db.t_LeaderMeeting.Where(n => n.LeaderID == id && n.IsDelete == 0).ToList();
            if (queryLeader.Count == 0)
            {
                try
                {
                    db.SaveChanges();
                    return Json(new MessageData(true, "删除成功"));
                }
                catch (Exception)
                {
                    return Json(new MessageData(false, "删除失败"));
                }
            }
            else
            {
                return Json(new MessageData(false, "该领导已有接访记录，无法删除"));
            }
        }
        #endregion

        #region 责任单位
        public ActionResult CompanyData()
        {
            return View();
        }

        [HttpPost] //获取责任单位数据
        public ActionResult GetCompany(int pageindex, int pagesize)
        {
            var temp = db.t_Responsibility.Where(n => n.Status == 0)
                            .OrderByDescending(n => n.TableId)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_Responsibility.Where(n => n.Status == 0).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("CompanyPartial", temp);
        }

        [HttpPost] //添加责任单位数据
        public ActionResult AddCompany(CompanyModel model)
        {
            t_Responsibility newCompany = new t_Responsibility();
            newCompany.UnitName = model.UnitName.Trim();
            db.t_Responsibility.Add(newCompany);

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception e)
            {
                return Json(new MessageData(false, "添加失败："+e.Message));
            }
        }

        [HttpPost] //修改责任单位数据
        public ActionResult EditCompany(CompanyModel model)
        {
            var newCompany = db.t_Responsibility.FirstOrDefault(n => n.TableId == model.ID);
            newCompany.UnitName = model.UnitName.Trim();

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost] //删除责任单位数据
        public ActionResult DelCompany(int id)
        {
            var queryCaseInfo = db.t_CaseInfo.Where(n => n.UnitType == id && n.IsDelete == 0).ToList();
            if (queryCaseInfo.Count == 0)
            {
                var newCompany = db.t_Responsibility.FirstOrDefault(n => n.TableId == id);
                newCompany.Status = 1;

                try
                {
                    db.SaveChanges();
                    return Json(new MessageData(true, "删除成功"));
                }
                catch (Exception)
                {
                    return Json(new MessageData(false, "删除失败"));
                }
            }
            else
            {
                return Json(new MessageData(false, "已有案件关联此责任单位，无法删除"));
            }
        }
        #endregion
    }
}