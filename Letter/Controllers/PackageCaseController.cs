﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;
using System.Linq.Expressions;

namespace Letter.Controllers
{
    public class PackageCaseController : BaseController
    {
        public ActionResult PackageCaseList()
        {
            return View();
        }

        public ActionResult PackageCaseDetail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetPackageCase(int pageindex, int pagesize)
        {
            string expression = Power();
            var sql = "select * from [dbo].[t_CaseInfo] where IsDelete = 0" + expression;
            var resultModel = db.Database.SqlQuery<PackageCaseInfoModel>(sql)
                .OrderByDescending(n=>n.ID)
                              .Skip(pagesize * (pageindex - 1))
                              .Take(pagesize).ToList();
            #region
            foreach (var item in resultModel)
            {
                var packageInfo = db.t_PackageCasePersonnel.OrderByDescending(r => r.ID).FirstOrDefault(r => r.CaseInfoID == item.ID);
                if (packageInfo != null)
                {
                    if (packageInfo.PrefectureLeaderName == null)
                    {
                        item.PrefectureLeaderName = "未设置";
                    }
                    else
                    {
                        item.PrefectureLeaderName = packageInfo.PrefectureLeaderName;
                        item.PrefectureLeaderPosition = packageInfo.PrefectureLeaderPosition;
                    }
                    if (packageInfo.OfficeLeaderName == null)
                    {
                        item.OfficeLeaderName = "未设置";
                    }
                    else
                    {
                        item.OfficeLeaderName = packageInfo.OfficeLeaderName;
                        item.OfficeLeaderPosition = packageInfo.OfficeLeaderPosition;
                    }
                    
                    if (packageInfo.OfficeDirectLeaderName == null)
                    {
                        item.OfficeDirectLeaderName = "未设置";
                    }
                    else
                    {
                        item.OfficeDirectLeaderName = packageInfo.OfficeDirectLeaderName;
                        item.OfficeDirectLeaderPosition = packageInfo.OfficeDirectLeaderPosition;
                    }
                    
                    if (packageInfo.DirectLeaderName == null)
                    {
                        item.DirectLeaderName = "未设置";
                    }
                    else
                    {
                        item.DirectLeaderName = packageInfo.DirectLeaderName;
                        item.DirectLeaderPosition = packageInfo.DirectLeaderPosition;
                    }
                    if (packageInfo.StabilityLeaderName == null)
                    {
                        item.StabilityLeaderName = "未设置";
                    }
                    else
                    {
                        item.StabilityLeaderName = packageInfo.StabilityLeaderName;
                        item.StabilityLeaderPosition = packageInfo.StabilityLeaderPosition;
                    }
                    if (packageInfo.BasicLeaderName == null)
                    {
                        item.BasicLeaderName = "未设置";
                    }
                    else
                    {
                        item.BasicLeaderName = packageInfo.BasicLeaderName;
                        item.BasicLeaderPosition = packageInfo.BasicLeaderPosition;
                    }
                    if (packageInfo.BasicPoliceName == null)
                    {
                        item.BasicPoliceName = "未设置";
                    }
                    else
                    {
                        item.BasicPoliceName = packageInfo.BasicPoliceName;
                        item.BasicPolicePosition = packageInfo.BasicPolicePosition;
                    }
                }
                else
                {
                    item.PrefectureLeaderName = "未设置";
                    item.OfficeLeaderName = "未设置";
                    item.OfficeDirectLeaderName = "未设置";
                    item.DirectLeaderName = "未设置";
                    item.StabilityLeaderName = "未设置";
                    item.BasicLeaderName = "未设置";
                    item.BasicPoliceName = "未设置";
                }

            }
            #endregion

            ViewData["roleList"] = Power(1);
            ViewData["total"] = db.Database.SqlQuery<PackageCaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("PackageCasePartial", resultModel);
        }

        [HttpPost]
        public ActionResult QueryPackageCase(int pageindex, int pagesize, string AppealNumber, string AppealTitle,string PetitionName)
        {
            string expression = Power();
            string where = "";
            if (PetitionName.Trim() != "")
            {
                where += " and PetitionName like '%" + PetitionName.Trim() + "%'";
            }
            var sql = db.Database.SqlQuery<PackageCaseInfoModel>("select * from [dbo].[t_CaseInfo] where IsDelete = 0 " + where+ expression );
            var resultModel = sql.OrderByDescending(n => n.ID).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();

            #region
            foreach (var item in resultModel)
            {
                var packageInfo = db.t_PackageCasePersonnel.OrderByDescending(r => r.ID).FirstOrDefault(r => r.CaseInfoID == item.ID);
                if (packageInfo != null)
                {
                    if (packageInfo.PrefectureLeaderName == null)
                    {
                        item.PrefectureLeaderName = "未设置";
                    }
                    else
                    {
                        item.PrefectureLeaderName = packageInfo.PrefectureLeaderName;
                        item.PrefectureLeaderPosition = packageInfo.PrefectureLeaderPosition;
                    }
                    if (packageInfo.OfficeLeaderName == null)
                    {
                        item.OfficeLeaderName = "未设置";
                    }
                    else
                    {
                        item.OfficeLeaderName = packageInfo.OfficeLeaderName;
                        item.OfficeLeaderPosition = packageInfo.OfficeLeaderPosition;
                    }

                    if (packageInfo.OfficeDirectLeaderName == null)
                    {
                        item.OfficeDirectLeaderName = "未设置";
                    }
                    else
                    {
                        item.OfficeDirectLeaderName = packageInfo.OfficeDirectLeaderName;
                        item.OfficeDirectLeaderPosition = packageInfo.OfficeDirectLeaderPosition;
                    }

                    if (packageInfo.DirectLeaderName == null)
                    {
                        item.DirectLeaderName = "未设置";
                    }
                    else
                    {
                        item.DirectLeaderName = packageInfo.DirectLeaderName;
                        item.DirectLeaderPosition = packageInfo.DirectLeaderPosition;
                    }
                    if (packageInfo.StabilityLeaderName == null)
                    {
                        item.StabilityLeaderName = "未设置";
                    }
                    else
                    {
                        item.StabilityLeaderName = packageInfo.StabilityLeaderName;
                        item.StabilityLeaderPosition = packageInfo.StabilityLeaderPosition;
                    }
                    if (packageInfo.BasicLeaderName == null)
                    {
                        item.BasicLeaderName = "未设置";
                    }
                    else
                    {
                        item.BasicLeaderName = packageInfo.BasicLeaderName;
                        item.BasicLeaderPosition = packageInfo.BasicLeaderPosition;
                    }
                    if (packageInfo.BasicPoliceName == null)
                    {
                        item.BasicPoliceName = "未设置";
                    }
                    else
                    {
                        item.BasicPoliceName = packageInfo.BasicPoliceName;
                        item.BasicPolicePosition = packageInfo.BasicPolicePosition;
                    }
                }
                else
                {
                    item.PrefectureLeaderName = "未设置";
                    item.OfficeLeaderName = "未设置";
                    item.OfficeDirectLeaderName = "未设置";
                    item.DirectLeaderName = "未设置";
                    item.StabilityLeaderName = "未设置";
                    item.BasicLeaderName = "未设置";
                    item.BasicPoliceName = "未设置";
                }

            }
            #endregion
            ViewData["roleList"] = Power(1);
            ViewData["total"] = sql.Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("PackageCasePartial", resultModel);
        }

        public ActionResult AddPackageCase()
        {
            return View();
        }

        /// <summary>
        /// 添加包案信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddPackageCase(PackageCasePersonnelModel model)
        {
            t_PackageCasePersonnel tbpersonnel = new t_PackageCasePersonnel();
            tbpersonnel.CaseInfoID = model.CaseInfoID;
            tbpersonnel.PrefectureLeaderName = model.PrefectureLeaderName;
            tbpersonnel.PrefectureLeaderPosition = model.PrefectureLeaderPosition;
            tbpersonnel.OfficeLeaderName = model.OfficeLeaderName;
            tbpersonnel.OfficeLeaderPosition = model.OfficeLeaderPosition;
            tbpersonnel.OfficeDirectLeaderName = model.OfficeDirectLeaderName;
            tbpersonnel.OfficeDirectLeaderPosition = model.OfficeDirectLeaderPosition;
            tbpersonnel.DirectLeaderName = model.DirectLeaderName;
            tbpersonnel.DirectLeaderPosition = model.DirectPosition;
            tbpersonnel.StabilityLeaderName = model.StabilityLeaderName;
            tbpersonnel.StabilityLeaderPosition = model.StabilityLeaderPosition;
            tbpersonnel.BasicLeaderName = model.BasicLeaderName;
            tbpersonnel.BasicLeaderPosition = model.BasicLeaderPosition;
            tbpersonnel.BasicPoliceName = model.BasicPoliceName;
            tbpersonnel.BasicPolicePosition = model.BasicPolicePosition;

            db.t_PackageCasePersonnel.Add(tbpersonnel);
            try
            {
                db.SaveChanges();
                //return RedirectToAction("CaseInfoList");
                return Json(new MessageData(true, "添加成功"));

            }
            catch (Exception)
            {
                //return RedirectToAction("CaseInfoList");
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost] //修改包案信息
        public ActionResult EditUser(PackageCasePersonnelModel model)
        {
            var Personneledit = db.t_PackageCasePersonnel.FirstOrDefault(n => n.ID == model.ID);
            Personneledit.PrefectureLeaderName = model.PrefectureLeaderName.Trim();
            Personneledit.PrefectureLeaderPosition = model.PrefectureLeaderPosition.Trim();
            Personneledit.OfficeLeaderName = model.OfficeLeaderName.Trim();
            Personneledit.OfficeLeaderPosition = model.OfficeLeaderPosition.Trim();
            Personneledit.OfficeDirectLeaderName = model.OfficeDirectLeaderName.Trim();
            Personneledit.OfficeDirectLeaderPosition = model.OfficeDirectLeaderPosition.Trim();
            Personneledit.DirectLeaderName = model.DirectLeaderName.Trim();
            Personneledit.DirectLeaderPosition = model.DirectPosition;
            Personneledit.StabilityLeaderName = model.StabilityLeaderName;
            Personneledit.StabilityLeaderPosition = model.StabilityLeaderPosition;
            Personneledit.BasicLeaderName = model.BasicLeaderName;
            Personneledit.BasicLeaderPosition = model.BasicLeaderPosition;
            Personneledit.BasicPoliceName = model.BasicPoliceName;
            Personneledit.BasicPolicePosition = model.BasicPolicePosition;

            try
            {
                db.SaveChanges();

                t_PackageCaseRecord tbRecord = new t_PackageCaseRecord();
                tbRecord.PackageCaseID = model.ID;
                tbRecord.UpdateDate = DateTime.Now;
                tbRecord.UpdatePerson = CurrentUser.LoginName;

                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        /// <summary>
        /// 添加包案记录
        /// </summary>
        /// <param name="id"></param>
        /// <param name="beforeName"></param>
        /// <param name="beforePosition"></param>
        /// <param name="type"></param>
        /// <param name="afterName"></param>
        /// <param name="afterPosition"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddRecord(int id , string beforeName, string beforePosition, int type, string afterName, string afterPosition)
        {
            t_PackageCaseRecord tbRecord = new t_PackageCaseRecord();
            tbRecord.PackageCaseID = id;
            tbRecord.PackageCaseType = type;
            tbRecord.BeforeName = beforeName;
            tbRecord.BeforePosition = beforePosition;
            tbRecord.AfterName = afterName;
            tbRecord.AfterPosition = afterPosition;
            tbRecord.UpdateDate = DateTime.Now;
            tbRecord.UpdatePerson = CurrentUser.LoginName;
            db.t_PackageCaseRecord.Add(tbRecord);
            try
            {
                db.SaveChanges();

                var Personneledit = db.t_PackageCasePersonnel.FirstOrDefault(n => n.CaseInfoID == id);
                if(Personneledit==null)
                {
                    t_PackageCasePersonnel tbpersonnel = new t_PackageCasePersonnel();
                    tbpersonnel.CaseInfoID = id;
                    if (type == 1)
                    {
                        tbpersonnel.PrefectureLeaderName = afterName;
                        tbpersonnel.PrefectureLeaderPosition = afterPosition;
                    }
                    else if (type == 2)
                    {
                        tbpersonnel.OfficeLeaderName = afterName;
                        tbpersonnel.OfficeLeaderPosition = afterPosition;
                    }
                    else if (type == 3)
                    {
                        tbpersonnel.DirectLeaderName = afterName;
                        tbpersonnel.DirectLeaderPosition = afterPosition;
                    }
                    else if (type == 4)
                    {
                        tbpersonnel.StabilityLeaderName = afterName;
                        tbpersonnel.StabilityLeaderPosition = afterPosition;
                    }
                    else if (type == 5)
                    {
                        tbpersonnel.BasicLeaderName = afterName;
                        tbpersonnel.BasicLeaderPosition = afterPosition;
                    }
                    else if (type == 6)
                    {
                        tbpersonnel.BasicPoliceName = afterName;
                        tbpersonnel.BasicPolicePosition = afterPosition;
                    }
                    else if (type == 7)
                    {
                        tbpersonnel.OfficeDirectLeaderName = afterName;
                        tbpersonnel.OfficeDirectLeaderPosition = afterPosition;
                    }
                    db.t_PackageCasePersonnel.Add(tbpersonnel);
                    db.SaveChanges();
                }
                else
                {
                    if (type == 1)
                    {
                        Personneledit.PrefectureLeaderName = afterName;
                        Personneledit.PrefectureLeaderPosition = afterPosition;
                    }
                    else if (type == 2)
                    {
                        Personneledit.OfficeLeaderName = afterName;
                        Personneledit.OfficeLeaderPosition = afterPosition;
                    }
                    else if (type == 3)
                    {
                        Personneledit.DirectLeaderName = afterName;
                        Personneledit.DirectLeaderPosition = afterPosition;
                    }
                    else if (type == 4)
                    {
                        Personneledit.StabilityLeaderName = afterName;
                        Personneledit.StabilityLeaderPosition = afterPosition;
                    }
                    else if (type == 5)
                    {
                        Personneledit.BasicLeaderName = afterName;
                        Personneledit.BasicLeaderPosition = afterPosition;
                    }
                    else if (type == 6)
                    {
                        Personneledit.BasicPoliceName = afterName;
                        Personneledit.BasicPolicePosition = afterPosition;
                    }
                    else if (type == 7)
                    {
                        Personneledit.OfficeDirectLeaderName = afterName;
                        Personneledit.OfficeDirectLeaderPosition = afterPosition;
                    }
                    db.SaveChanges();
                }
                
                return Json(new MessageData(true, "修改成功"));

            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        /// <summary>
        /// 显示包案修改记录
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RecordList(int pageindex, int pagesize, int id,int type)
        {
            var RecordList = db.t_PackageCaseRecord.Where(t => t.PackageCaseID == id && t.PackageCaseType == type).OrderByDescending(r => r.ID)
                              .Skip(pagesize * (pageindex - 1))
                              .Take(pagesize).ToList();
            ViewData["total"] = db.t_PackageCaseRecord.Where(t => t.PackageCaseID == id).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;
            return PartialView("PackageModalPartial", RecordList.ToList());
        }
    }
}