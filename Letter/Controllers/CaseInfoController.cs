﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;
using Business;
using System.Linq.Expressions;
using System.Data;

namespace Letter.Controllers
{
    public class CaseInfoController : BaseController
    {
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult CaseInfoList()
        {
            var letterList = db.t_CaseInfo.Where(t => t.IsDelete == 0).ToList();
            var areaList = db.t_County.Where(n => n.Status == 0).ToList();
            ViewBag.TownshipType = areaList;

            var unitList = db.t_Responsibility.Where(n => n.Status == 0).ToList();
            ViewBag.UnitType = unitList;
            return View(letterList);
        }
        [HttpPost]
        public ActionResult GetCaseInfo(int pageindex, int pagesize)
        {
            string expression = Power();
            string sql = "SELECT ID,[TownshipType],[UnitType],[RelateUnitID],[RelateUnitName],[RelateAreaID],[RelateAreaName],[AppealNumber],[AppealTitle],[PetitionName] ,[PetitionTel],[IsLawLitigation],[IsRevisit],[IsReconcile],[IsStopPetition] FROM[LetterInfo].[dbo].[t_CaseInfo] where IsDelete = 0" + expression;
            var temp = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("CaseInfoPartial", temp);
        }

        [HttpPost]
        public ActionResult QueryCaseInfo(int pageindex, int pagesize, string PetitionName, string TownshipType, string UnitType)
        {
            string expression = Power();

            string where = string.Empty;
            if(TownshipType == null)
            {
                TownshipType = "0";
            }
            if (TownshipType!="0")
            {
                where += " and RelateAreaName like '%" + TownshipType + "%'";
            }
            if (UnitType != "0" && UnitType !=null)
            {
                where += " and RelateUnitName like '%" + UnitType + "%'";
            }
            if (PetitionName.Trim() != "")
            {
                where += " and PetitionName like '%" + PetitionName + "%'";
            }

            string sql = "SELECT ID,[TownshipType],[UnitType],[RelateUnitID],[RelateUnitName],[RelateAreaID],[RelateAreaName],[AppealNumber],[AppealTitle],[PetitionName] ,[PetitionTel],[IsLawLitigation],[IsRevisit],[IsReconcile],[IsStopPetition] FROM[LetterInfo].[dbo].[t_CaseInfo] where IsDelete = 0"+ expression + where;
            var queryList = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("CaseInfoPartial", queryList);
        }

  
        public ActionResult CaseInfoAdd()
        {    
            if(CurrentUser != null)
            {
                var areaList = db.t_County.Where(n => n.Status == 0).ToList();
                //if (CurrentUser.UserRoleList.Contains("3"))
                //{
                //    areaList = db.t_County.Where(n => n.Status == 0 && n.CountyId == CurrentUser.UserArea).ToList();
                //}
                //else
                //{

                //}
                ViewData["TownshipType"] = new SelectList(areaList, "CountyId", "CountyName");

                var unitList = db.t_Responsibility.Where(n => n.Status == 0).ToList();
                ViewData["UnitType"] = new SelectList(unitList, "TableId", "UnitName");
            }
         
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CaseInfoAdd(CaseInfoModel model)
        {
            var tbList = db.t_CaseInfo.Where(n => n.IsDelete == 0).ToList();
            string number = string.Empty;
            string title = string.Empty;
            if(tbList.Count == 0)
            {
                number = "1000001";
            }
            else
            {
                var tbNumber = db.t_CaseInfo.Where(n => n.IsDelete == 0).OrderByDescending(n => n.ID).Take(1).ToList();
                number =Convert.ToString(Convert.ToInt32(tbNumber[0].AppealNumber) + 1);

                //var tbNumber = db.t_CaseInfo.Where( n=> n.IsDelete == 0).OrderByDescending(n => n.ID).Take(1).ToDictionary(key =>key.AppealNumber);
                //number = tbNumber.FirstOrDefault(r => r.Key == r.Key).Value.AppealNumber + 1;
            }

            //title = number + " - " + model.PetitionName + " - " + model.UnitName + " - " + model.CountyName;
            title = number + " - " + model.PetitionName;
            if (model.RelateUnitName.Equals("请选择"))
            {
                model.RelateUnitName = string.Empty;
            }
            else
            {
                title += " - " + model.RelateUnitName;
            }

            if (model.RelateAreaName.Equals("请选择"))
            {
                model.RelateAreaName = string.Empty;
            }
            else
            {
                title += " - " + model.RelateAreaName;
            }

            t_CaseInfo tbCase = new t_CaseInfo();
            tbCase.PetitionName = model.PetitionName;
            tbCase.PetitionIDNo = model.PetitionIDNo;
            tbCase.TownshipType = model.TownshipType;
            tbCase.UnitType = model.UnitType;
            tbCase.PetitionAddress = model.PetitionAddress;
            tbCase.PetitionTel = model.PetitionTel;
            tbCase.IsLawLitigation = model.IsLawLitigation;
            tbCase.PetitionType = model.PetitionType;
            tbCase.IsRevisit = model.IsRevisit;
            tbCase.PeopleCount = model.PeopleCount;
            tbCase.CheckingDate = model.CheckingDate;
            tbCase.AppealTitle = title;
            tbCase.AppealContent = model.AppealContent;
            tbCase.IsReceptionSite = model.IsReceptionSite;
            tbCase.IsSuperior = model.IsSuperior;
            tbCase.IsPackageCase = model.IsPackageCase;
            tbCase.IsAccumulatedCases = model.IsAccumulatedCases;
            tbCase.IsReconcile = 1;
            tbCase.ReconcileDate = model.ReconcileDate;
            tbCase.ReconcileContent = model.ReconcileContent;
            tbCase.IsStopPetition = 1;
            tbCase.StopPetitionDate = model.StopPetitionDate;
            tbCase.StopPetitionLetter = model.StopPetitionLetter;
            tbCase.AppealNumber = number;
            tbCase.IsDelete = 0;
            tbCase.ReceptionSite = model.ReceptionSite;
            tbCase.ReceptionSiteDate = model.ReceptionSiteDate;
            tbCase.PackageCaseRemark = model.PackageCaseRemark;
            tbCase.AccumulatedCaseRemark = model.AccumulatedCaseRemark;
            tbCase.RelateUnitID = model.RelateUnitID;
            tbCase.RelateUnitName = model.RelateUnitName;
            tbCase.RelateAreaID = model.RelateAreaID;
            tbCase.RelateAreaName = model.RelateAreaName;
            db.t_CaseInfo.Add(tbCase);
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception)
            {
                return RedirectToAction("CaseInfoList");
            }
        }
        /// <summary>
        /// 获取案件信息数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CaseInfoEdit(int? id)
        {
            if (CurrentUser == null || id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                var areaList = db.t_County.Where(n => n.Status == 0).ToList();
                ViewData["TownshipType"] = new SelectList(areaList, "CountyId", "CountyName");

                var unitList = db.t_Responsibility.Where(n => n.Status == 0).ToList();
                ViewData["UnitType"] = new SelectList(unitList, "TableId", "UnitName");

                var caseedit = db.t_CaseInfo.FirstOrDefault(n => n.ID == id);
                CaseInfoModel casemodel = new CaseInfoModel();
                casemodel.PetitionName = caseedit.PetitionName;
                casemodel.PetitionIDNo = caseedit.PetitionIDNo;
                casemodel.TownshipType = caseedit.TownshipType;
                casemodel.UnitType = caseedit.UnitType;
                casemodel.PetitionAddress = caseedit.PetitionAddress;
                casemodel.PetitionTel = caseedit.PetitionTel;
                casemodel.IsLawLitigation = caseedit.IsLawLitigation;
                casemodel.PetitionType = caseedit.PetitionType;
                casemodel.IsRevisit = caseedit.IsRevisit;
                casemodel.PeopleCount = caseedit.PeopleCount;
                casemodel.CheckingDate = caseedit.CheckingDate;
                casemodel.AppealTitle = caseedit.AppealTitle;
                casemodel.AppealContent = caseedit.AppealContent;
                casemodel.IsReceptionSite = caseedit.IsReceptionSite;
                casemodel.IsSuperior = caseedit.IsSuperior;
                casemodel.IsPackageCase = caseedit.IsPackageCase;
                casemodel.IsAccumulatedCases = caseedit.IsAccumulatedCases;
                casemodel.IsReconcile = caseedit.IsReconcile;
                casemodel.ReconcileDate = caseedit.ReconcileDate;
                casemodel.ReconcileContent = caseedit.ReconcileContent;
                casemodel.IsStopPetition = caseedit.IsStopPetition;
                casemodel.StopPetitionDate = caseedit.StopPetitionDate;
                casemodel.StopPetitionLetter = caseedit.StopPetitionLetter;
                casemodel.AppealNumber = caseedit.AppealNumber;
                casemodel.ReceptionSite = caseedit.ReceptionSite;
                casemodel.ReceptionSiteDate = caseedit.ReceptionSiteDate;
                casemodel.PackageCaseRemark = caseedit.PackageCaseRemark;
                casemodel.AccumulatedCaseRemark = caseedit.AccumulatedCaseRemark;
                casemodel.RelateUnitID = caseedit.RelateUnitID;
                casemodel.RelateAreaID = caseedit.RelateAreaID;

                ViewData["TownshipTypeVal"] = casemodel.TownshipType;
                ViewData["UnitTypeVal"] = casemodel.UnitType;

                ViewData["PetitionTypeVal"] = casemodel.PetitionType;
                ViewData["IsRevisitVal"] = casemodel.IsRevisit;
                return View(casemodel);
            }
           
            
        }

        /// <summary>
        /// 修改案件信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateCase"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CaseInfoEdit(int id, CaseInfoModel updateCase)
        {
            var caseedit = db.t_CaseInfo.FirstOrDefault(n => n.ID == id);
            string title = string.Empty;

            title = caseedit.AppealNumber + " - " + updateCase.PetitionName;
            if (updateCase.RelateUnitName != "请选择")
            {
                title += " - " + updateCase.RelateUnitName;
            }
            if (updateCase.RelateAreaName != "请选择")
            {
                title += " - " + updateCase.RelateAreaName;
            }

            caseedit.PetitionName = updateCase.PetitionName;
            caseedit.PetitionIDNo = updateCase.PetitionIDNo;
            caseedit.TownshipType = updateCase.TownshipType;
            caseedit.UnitType = updateCase.UnitType;
            caseedit.PetitionAddress = updateCase.PetitionAddress;
            caseedit.PetitionTel = updateCase.PetitionTel;
            caseedit.IsLawLitigation = updateCase.IsLawLitigation;
            caseedit.PetitionType = updateCase.PetitionType;
            caseedit.IsRevisit = updateCase.IsRevisit;
            caseedit.PeopleCount = updateCase.PeopleCount;
            caseedit.CheckingDate = updateCase.CheckingDate;
            caseedit.AppealTitle = title;
            caseedit.AppealContent = updateCase.AppealContent;
            caseedit.IsReceptionSite = updateCase.IsReceptionSite;
            caseedit.IsSuperior = updateCase.IsSuperior;
            caseedit.IsPackageCase = updateCase.IsPackageCase;
            caseedit.IsAccumulatedCases = updateCase.IsAccumulatedCases;
            //caseedit.IsReconcile = updateCase.IsReconcile;
            //caseedit.ReconcileDate = updateCase.ReconcileDate;
            //caseedit.ReconcileContent = updateCase.ReconcileContent;
            //caseedit.IsStopPetition = updateCase.IsStopPetition;
            //caseedit.StopPetitionDate = updateCase.StopPetitionDate;
            //caseedit.StopPetitionLetter = updateCase.StopPetitionLetter;
            caseedit.ReceptionSite = updateCase.ReceptionSite;
            caseedit.ReceptionSiteDate = updateCase.ReceptionSiteDate;
            caseedit.PackageCaseRemark = updateCase.PackageCaseRemark;
            caseedit.AccumulatedCaseRemark = updateCase.AccumulatedCaseRemark;
            caseedit.RelateUnitID = updateCase.RelateUnitID;
            if (updateCase.RelateUnitName.Equals("请选择"))
                caseedit.RelateUnitName = null;
            else
                caseedit.RelateUnitName = updateCase.RelateUnitName;
            caseedit.RelateAreaID = updateCase.RelateAreaID;
            if (updateCase.RelateAreaName.Equals("请选择"))
                caseedit.RelateAreaName = null;
            else
                caseedit.RelateAreaName = updateCase.RelateAreaName;
            try
            {
                db.SaveChanges();
                return RedirectToAction("CaseInfoList");
                //return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                //return Json(new MessageData(false, "修改失败"));
                return RedirectToAction("CaseInfoList");
            }
        }
        /// <summary>
        /// 删除案件信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CaseInfoDelete(int id, CaseInfoModel model)
        {
            var delcase = db.t_CaseInfo.FirstOrDefault(n => n.ID == id);
            delcase.IsDelete = 1;
            db.SaveChanges();
            return Json(new MessageData(true, "删除成功"));
            //return RedirectToAction("CaseInfoList");
        }

        /// <summary>
        /// 案件信息详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CaseInfoDetail(int id, CaseInfoModel casemodel)
        {
            var caseedit = db.t_CaseInfo.FirstOrDefault(n => n.ID == id);

            casemodel.PetitionName = caseedit.PetitionName;
            casemodel.PetitionIDNo = caseedit.PetitionIDNo;
            casemodel.TownshipType = caseedit.TownshipType;
            casemodel.UnitType = caseedit.UnitType;
            casemodel.PetitionAddress = caseedit.PetitionAddress;
            casemodel.PetitionTel = caseedit.PetitionTel;
            casemodel.IsLawLitigation = caseedit.IsLawLitigation;
            casemodel.PetitionType = caseedit.PetitionType;
            casemodel.IsRevisit = caseedit.IsRevisit;
            casemodel.PeopleCount = caseedit.PeopleCount;
            casemodel.CheckingDate = caseedit.CheckingDate;
            casemodel.AppealTitle = caseedit.AppealTitle;
            casemodel.AppealContent = caseedit.AppealContent;
            casemodel.IsReceptionSite = caseedit.IsReceptionSite;
            casemodel.IsSuperior = caseedit.IsSuperior;
            casemodel.IsPackageCase = caseedit.IsPackageCase;
            casemodel.IsAccumulatedCases = caseedit.IsAccumulatedCases;
            casemodel.IsReconcile = caseedit.IsReconcile;
            casemodel.ReconcileDate = caseedit.ReconcileDate;
            casemodel.ReconcileContent = caseedit.ReconcileContent;
            casemodel.IsStopPetition = caseedit.IsStopPetition;
            casemodel.StopPetitionDate = caseedit.StopPetitionDate;
            casemodel.StopPetitionLetter = caseedit.StopPetitionLetter;
            casemodel.AppealNumber = caseedit.AppealNumber;
            casemodel.ReceptionSite = caseedit.ReceptionSite;
            casemodel.ReceptionSiteDate = caseedit.ReceptionSiteDate;
            casemodel.PackageCaseRemark = caseedit.PackageCaseRemark;
            casemodel.AccumulatedCaseRemark = caseedit.AccumulatedCaseRemark;
            return View(casemodel);
        }

        [HttpPost]
        public ActionResult CaseInfoExport(int id,string num)
        {
            string path = Server.MapPath("~/ExportExcel/");
            path += string.Format("{0}{1}{2}", "案件编号", num, ".xls");
            try
            {
                DataSet ds = GetInfo(id);
                ExportHelp.testExcel(ds, path);
                return Json(new MessageData(true, "成功"));
            }
            catch(Exception ex)
            {
                return Json(new MessageData(true, ex.ToString()));
            }
        
        
            //return RedirectToAction("CaseInfoList");
        }

        public ActionResult CaseInfoExportAll(string PetitionName, string TownshipType, string UnitType, string OrderBy)
        {
            string path = Server.MapPath("~/ExportExcel/案件管理.xls");
            string where = string.Empty;
            string sql = " and {0} like '%{1}%'";
            if (TownshipType == null)
            {
                TownshipType = "0";
            }
            if (TownshipType != "0")
            {
                //where += " and RelateAreaName like '%" + TownshipType + "%'";
                where += string.Format(sql, "RelateAreaName", TownshipType);
            }
            if (UnitType != "0" && UnitType != null)
            {
                //where += " and RelateUnitName like '%" + UnitType + "%'";
                where += string.Format(sql, "RelateUnitName", UnitType);
            }
            if (PetitionName.Trim() != "")
            {
                // where += " and PetitionName like '%" + PetitionName + "%'";
                where += string.Format(sql, "PetitionName", PetitionName);
            }

            where = string.Format(where + " order by {0}", OrderBy);

            try
            {
                DataSet ds = GetInfo(where);
                ExportHelp.testExcel(ds, path,true);
                return Json(new MessageData(true, "成功"));
            }
            catch (Exception ex)
            {
                return Json(new MessageData(true, ex.ToString()));
            }
        }
    }
}