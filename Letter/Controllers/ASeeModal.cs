﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using Model;
namespace Letter.Controllers
{
    public class ASeeModalController : BaseController
    {
       [HttpPost] //案件基本信息
        public ActionResult GetSeeModalTb1(int id)
        {
            string sql = "SELECT ID,[AppealNumber],[RelateUnitName],[RelateAreaName],[PetitionName],AppealTitle,AppealContent,PetitionIDNo,[PetitionTel],PetitionAddress,[IsLawLitigation],PetitionType,[IsRevisit],PeopleCount,CheckingDate,IsReceptionSite,ReceptionSite,ReceptionSiteDate,IsSuperior,IsPackageCase,PackageCaseRemark,IsAccumulatedCases,AccumulatedCaseRemark FROM[LetterInfo].[dbo].[t_CaseInfo] where ID =" + id;
            var temp = db.Database.SqlQuery<CaseInfoModel>(sql).ToList();

            return PartialView("SeeModalTb1", temp);
        }

        [HttpPost] //党政领导接访信息
        public ActionResult GetSeeModalTb2(int id)
        {
            var temp = db.t_LeaderMeeting.Where(n => n.IsDelete == 0 && n.CaseInfoID == id).ToList();

            return PartialView("SeeModalTb2", temp);
        }

        [HttpPost]
        public ActionResult GetSeeModalTb3(int id)
        {
            var sql = "select * from [dbo].[t_CaseInfo] where IsDelete = 0 and ID =" + id;
            var temp = db.Database.SqlQuery<PackageCaseInfoModel>(sql).ToList();

            #region
            foreach (var item in temp)
            {
                var packageInfo = db.t_PackageCasePersonnel.OrderByDescending(r => r.ID).FirstOrDefault(r => r.CaseInfoID == item.ID);
                if (packageInfo != null)
                {
                    if (packageInfo.PrefectureLeaderName == null)
                    {
                        item.PrefectureLeaderName = "未设置";
                        item.PrefectureLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.PrefectureLeaderName = packageInfo.PrefectureLeaderName;
                        item.PrefectureLeaderPosition = packageInfo.PrefectureLeaderPosition;
                    }
                    if (packageInfo.OfficeLeaderName == null)
                    {
                        item.OfficeLeaderName = "未设置";
                        item.OfficeLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.OfficeLeaderName = packageInfo.OfficeLeaderName;
                        item.OfficeLeaderPosition = packageInfo.OfficeLeaderPosition;
                    }

                    if (packageInfo.OfficeDirectLeaderName == null)
                    {
                        item.OfficeDirectLeaderName = "未设置";
                        item.OfficeDirectLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.OfficeDirectLeaderName = packageInfo.OfficeDirectLeaderName;
                        item.OfficeDirectLeaderPosition = packageInfo.OfficeDirectLeaderPosition;
                    }

                    if (packageInfo.DirectLeaderName == null)
                    {
                        item.DirectLeaderName = "未设置";
                        item.DirectLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.DirectLeaderName = packageInfo.DirectLeaderName;
                        item.DirectLeaderPosition = packageInfo.DirectLeaderPosition;
                    }
                    if (packageInfo.StabilityLeaderName == null)
                    {
                        item.StabilityLeaderName = "未设置";
                        item.StabilityLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.StabilityLeaderName = packageInfo.StabilityLeaderName;
                        item.StabilityLeaderPosition = packageInfo.StabilityLeaderPosition;
                    }
                    if (packageInfo.BasicLeaderName == null)
                    {
                        item.BasicLeaderName = "未设置";
                        item.BasicLeaderPosition = "未设置";
                    }
                    else
                    {
                        item.BasicLeaderName = packageInfo.BasicLeaderName;
                        item.BasicLeaderPosition = packageInfo.BasicLeaderPosition;
                    }
                    if (packageInfo.BasicPoliceName == null)
                    {
                        item.BasicPoliceName = "未设置";
                        item.BasicPolicePosition = "未设置";
                    }
                    else
                    {
                        item.BasicPoliceName = packageInfo.BasicPoliceName;
                        item.BasicPolicePosition = packageInfo.BasicPolicePosition;
                    }
                }
                else
                {
                    item.PrefectureLeaderName = "未设置";
                    item.OfficeLeaderName = "未设置";
                    item.OfficeDirectLeaderName = "未设置";
                    item.DirectLeaderName = "未设置";
                    item.StabilityLeaderName = "未设置";
                    item.BasicLeaderName = "未设置";
                    item.BasicPoliceName = "未设置";    
                    item.PrefectureLeaderPosition = "未设置";
                    item.OfficeLeaderPosition = "未设置";
                    item.OfficeDirectLeaderPosition = "未设置";
                    item.DirectLeaderPosition = "未设置";
                    item.StabilityLeaderPosition = "未设置";
                    item.BasicLeaderPosition = "未设置";
                    item.BasicPolicePosition = "未设置";
                }

            }
            #endregion

            return PartialView("SeeModalTb3", temp);
        }

        [HttpPost]
        public ActionResult GetSeeModalTb4(int id)
        {
            var temp = db.t_Transact.Where(n => n.CaseInfoID == id && n.TransactType != 99).ToList();

            return PartialView("SeeModalTb4", temp);
        }

        [HttpPost]
        public ActionResult GetSeeModalTb5(int id)
        {
            var temp = db.t_Record.Where(n => n.CaseInfoID == id).ToList();
            foreach (var item in temp)
            {
                switch (item.RecordType)
                {
                    case "0 ":
                        item.RecordType = "赴省";
                        break;
                    case "1 ":
                        item.RecordType = "进京";
                        break;
                    case "3 ":
                        item.RecordType = "到市";
                        break;
                    case "4 ":
                        item.RecordType = "到县";
                        break;
                    case "5 ":
                        item.RecordType = "退役军人事务部";
                        break;
                    default:
                        item.RecordType = "非访";
                        break;
                }
            }
            return PartialView("SeeModalTb5", temp);
        }

        [HttpPost]
        public ActionResult GetSeeModalTb6(int id)
        {
            var temp = db.t_Bergabe.Where(n => n.CaseInfoID == id && n.IsDelete == 0).ToList();

            return PartialView("SeeModalTb6", temp);
        }

        [HttpPost]
        public ActionResult GetSeeModalTb7(int id)
        {
            string sql = "select l.ID,l.CaseInfoID,LeaderName,LeaderPosition,l.ReceptionDateBefore,l.ReceptionAddressBefore,l.ReceptionPersonBefore,l.ReceptionComment from [dbo].[t_LeaderReservation] l where l.IsDelete = 0 and l.CaseInfoID = " + id;
            var temp = db.Database.SqlQuery<LeaderReservationModel>(sql).ToList();

            return PartialView("SeeModalTb7", temp);
        }

        [HttpPost]
        public ActionResult GetRecordList(int id, int type)
        {
            var RecordList = db.t_PackageCaseRecord.Where(t => t.PackageCaseID == id && t.PackageCaseType == type).OrderByDescending(r => r.ID).ToList();

            return PartialView("SeeModalTb3Record", RecordList);
        }

    }
}