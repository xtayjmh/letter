﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using Model;

namespace Letter.Controllers
{
    public class BaseController : Controller
    {
        protected  LetterInfoEntities db = new LetterInfoEntities();
        protected t_User CurrentUser
        {
            get
            {
                if (Session["CurrentUser"] != null)
                {
                    return Session["CurrentUser"] as Model.t_User;
                }
                else
                {
                    return null;
                }
            }
        }

        public JsonResult UpFiles(string name)
        {
            var uFile = Request.Files[name];
            //var oStream = uFile.InputStream;

            DateTime date = DateTime.Now;
            var str = uFile.FileName.Split('.');
            string FileName = str[0] + "_" + date.ToString("yyyyMMddHHmmss") + "." + str[1];

            string sFileName = Server.MapPath("~/UploadFile/") + FileName;

            uFile.SaveAs(sFileName);

            var jsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(FileName);
            return Json(new MessageData(true, jsonStr));

        }

        public string Power()
        {
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = "";
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区
                {
                    expression = " and (RelateUnitID like '%" + CurrentUser.UserUnit + "%' or RelateAreaID like '%" + CurrentUser.UserArea + "%')";
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    expression = " and RelateUnitID  like '%" + CurrentUser.UserUnit + "%'";
                }

                else //片区
                {
                    expression = " and RelateAreaID like '%" + CurrentUser.UserArea + "%'";
                }
                
            }
            //expression += " order by [ID] desc";
            return expression;
        }
        public string Power(int num)
        {
            string roleList = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                roleList = "";
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区
                {
                    roleList = "34";
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    roleList = "4";
                }

                else //片区
                {
                    roleList = "3";
                }
            }
            return roleList;
        }

        public DataSet GetInfo(string where)
        {
            string expression = Power();
            expression += where;
            string sql = "EXEC dbo.ExportCaseInfo";
            sql += expression.Equals(string.Empty) ? "" : " @Role = \"" + expression +"\"";
            //DataSet ds = GetDataSet("EXEC dbo.ExportCaseInfo @Role = \"" + expression + "\"");
            DataSet ds = GetDataSet(sql);
            var aa = ds.Tables[0].Rows[0];
            return ds;
        }
        public DataSet GetInfo(int id)
        {
            string expression = Power();
            string sql = "EXEC dbo.ExportCaseInfo @CaseId=\"" + id + "\"";
            sql += expression.Equals(string.Empty) ? "" : ",@Role = \"" + expression + "\"";
            DataSet ds = GetDataSet(sql);
            //if (expression.Equals(string.Empty))
            //{
            //    DataSet ds = GetDataSet("EXEC dbo.ExportCaseInfo @CaseId=\"" + id + "\"");
            //                return ds;
            //}
            //else {
            //    DataSet ds = GetDataSet("EXEC dbo.ExportCaseInfo @CaseId=\"" + id + "\",@Role = \"" + expression + "\"");
            //    var aa = ds.Tables[0].Rows[0];
            //    return ds;
            //}
            return ds;
        }

        public DataSet GetDataSet(string sql, params SqlParameter[] para)
        {
            List<string> sql2 = new List<string>();
            for (int i = 0; i < para.Length; i++)
            {
                SqlParameter p = para[i];
                sql2.Add(p.ParameterName);
            }
            DataSet sd = new DataSet();
            using (var db = new LetterInfoEntities())
            {
                SqlConnection conn = db.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand(sql + " " + string.Join(",", sql2.ToArray()), conn))
                {
                    //cmd.Parameters.AddRange(para);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(sd);
                }
            }
            return sd;
        }


        public DataTable GetDataTable()
        {
            LetterInfoEntities context = new LetterInfoEntities();
            string expression = Power();
            SqlParameter[] sqlparams = new SqlParameter[2];
            sqlparams[0] = new SqlParameter("Role", expression);
            DataTable DataTable = SqlQueryForDataTatable(context.Database, "EXEC dbo.ExportCaseInfo @Role", sqlparams);

            return DataTable;

        }

        public DataTable SqlQueryForDataTatable(Database db, string sql, SqlParameter[] parameters)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = db.Connection.ConnectionString;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;

            if (parameters.Length > 0)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(item);
                }
            }

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            adapter.Fill(table);
            return table;
        }
    }
}