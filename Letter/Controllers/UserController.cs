﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult UserAdmin()
        {
            var areaList = db.t_County.Where(n => n.Status == 0).ToList();
            ViewData["userArea"] = new SelectList(areaList, "CountyId", "CountyName");

            var unitList = db.t_Responsibility.Where(n => n.Status == 0).ToList();
            ViewData["userUnit"] = new SelectList(unitList, "TableId", "UnitName");
            return View();
        }

        [HttpPost] //获取用户信息
        public ActionResult GetUser(int pageindex, int pagesize)
        {
            string roleList = string.Empty;
            string whereSql = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                roleList = "";
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区
                {
                    roleList = "34";
                    whereSql = "and TableId = " + CurrentUser.UserUnit + "and CountyId = " + CurrentUser.UserArea;
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    roleList = "4";
                    whereSql = "and TableId = " + CurrentUser.UserUnit;
                }

                else //片区
                {
                    roleList = "3";
                }
            }

            var userResult = db.Database.SqlQuery<t_UserCountyUnit>("SELECT [UserId],ISNULL(([CountyId]),0)as CountyId,ISNULL(([TableId]),0)as TableId,[UserName],[LoginName],[UserPassword],[UserPosition],[UserPhone],[UserIDNumber],[UserArea],[UserUnit],[CountyName],[UnitName],UserRole,[IsDelete]FROM[LetterInfo].[dbo].[t_User] LEFT OUTER JOIN  t_County on t_User.UserArea = t_County.CountyId LEFT OUTER JOIN t_Responsibility on t_User.UserUnit = t_Responsibility.[TableId] where IsDelete =  0 and LoginName <> 'admin'"+ whereSql + "")
            .OrderByDescending(n => n.UserId)
            .Skip(pagesize * (pageindex - 1))
            .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<t_UserCountyUnit>("SELECT [UserId],ISNULL(([CountyId]),0)as CountyId,ISNULL(([TableId]),0)as TableId,[UserName],[LoginName],[UserPassword],[UserPosition],[UserPhone],[UserIDNumber],[UserArea],[UserUnit],[CountyName],[UnitName],UserRole,[IsDelete]FROM[LetterInfo].[dbo].[t_User] LEFT OUTER JOIN  t_County on t_User.UserArea = t_County.CountyId LEFT OUTER JOIN t_Responsibility on t_User.UserUnit = t_Responsibility.[TableId] where IsDelete =  0 and LoginName <> 'admin'" + whereSql + "").Count();

            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UserPartial", userResult);
        }

        [HttpPost] //添加用户
        public ActionResult AddUser(UserModel model)
        {
            var loginName = db.t_User.FirstOrDefault(n => n.LoginName.Equals(model.LoginName));
           
            if (loginName == null)
            {
                t_User tbUser = new t_User();
                var UserUnit = model.UserUnit;
                tbUser.UserName = model.UserName.Trim();
                tbUser.LoginName = model.LoginName.Trim();
                tbUser.UserPassword = model.UserPassword.Trim();
                if (model.UserPosition == null)
                {
                    tbUser.UserPosition = "";
                }
                else
                {
                    tbUser.UserPosition = model.UserPosition.Trim();
                }
                if (model.UserPhone == null)
                {
                    tbUser.UserPhone = "";
                }
                else
                {
                    tbUser.UserPhone = model.UserPhone.Trim();
                }
                tbUser.UserIDNumber = model.UserIDNumber;  
                if (model.UserUnit == 999)
                {
                    tbUser.UserRole = "2,";
                    UserUnit = null;
                }
                if (model.UserArea != null)
                {
                    tbUser.UserRole += "3,";
                }
                if (model.UserUnit != null && model.UserUnit != 999)
                {
                    tbUser.UserRole += "4";
                }
                tbUser.UserUnit = UserUnit;
                tbUser.UserArea = model.UserArea;
                db.t_User.Add(tbUser);
                try
                {
                    db.SaveChanges();
                    return Json(new MessageData(true, "添加成功"));
                }
                catch (Exception)
                {
                    return Json(new MessageData(false, "添加失败"));
                }
            }
            else
            {
                return Json(new MessageData(true, "用户名重复"));
            }
            
        }

        [HttpPost] //修改用户信息
        public ActionResult EditUser(UserModel model)
        {
            var useredit = db.t_User.FirstOrDefault(n => n.UserId == model.UserId);

            useredit.UserName = model.UserName.Trim();
            useredit.LoginName = model.LoginName.Trim();
            useredit.UserPassword = model.UserPassword.Trim();
            if (model.UserPosition == null)
            {
                useredit.UserPosition = "";
            }
            else
            {
                useredit.UserPosition = model.UserPosition.Trim();
            }
            if (model.UserPhone == null)
            {
                useredit.UserPhone = "";
            }
            else
            {
                useredit.UserPhone = model.UserPhone.Trim();
            }
            useredit.UserIDNumber = model.UserIDNumber;
            useredit.UserArea = model.UserArea;
            useredit.UserUnit = model.UserUnit;
            useredit.UserRole = "";
            if (model.UserUnit == 999)
            {
                useredit.UserRole = "2,";
                useredit.UserUnit = null;
            }
            if (model.UserArea != null)
            {
                useredit.UserRole += "3,";
            }
            if (model.UserUnit != null && model.UserUnit != 999)
            {
                useredit.UserRole += "4";
            }

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost] //密码重置
        public ActionResult ResetUser(int id)
        {
            var useredit = db.t_User.FirstOrDefault(n => n.UserId == id);
            useredit.UserPassword = "123456";

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "密码重置成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "密码重置失败"));
            }
        }

        [HttpPost] //删除用户信息
        public ActionResult DelUser(int id)
        {
            var newUser = db.t_User.FirstOrDefault(n => n.UserId == id);
            newUser.IsDelete = 1;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public ActionResult UserEdit(int id)
        //{
        //    var useredit = db.t_User.FirstOrDefault(n => n.UserId == id);
        //    UserModel usermodel = new UserModel();
        //    usermodel.UserName = useredit.UserName;
        //    usermodel.LoginName = useredit.LoginName;
        //    usermodel.UserPassword = useredit.UserPassword;
        //    usermodel.UserPosition = useredit.UserPosition;
        //    usermodel.UserPhone = useredit.UserPhone;
        //    usermodel.UserIDNumber = useredit.UserIDNumber;
        //    usermodel.UserArea = useredit.UserArea;
        //    usermodel.UserUnit = useredit.UserUnit;
        //    return View(usermodel);
        //}

        //[HttpPost]
        //public ActionResult CaseInfoEdit(int id, UserModel updateUser)
        //{
        //    var useredit = db.t_User.FirstOrDefault(n => n.UserId == id);

        //    useredit.UserName = updateUser.UserName;
        //    useredit.LoginName = updateUser.LoginName;
        //    useredit.UserPassword = updateUser.UserPassword;
        //    useredit.UserPosition = updateUser.UserPosition;
        //    useredit.UserPhone = updateUser.UserPhone;
        //    useredit.UserIDNumber = updateUser.UserIDNumber;
        //    useredit.UserArea = updateUser.UserArea;
        //    useredit.UserUnit = updateUser.UserUnit;
        //    try
        //    {
        //        db.SaveChanges();
        //        return RedirectToAction("UserAdmin");
        //        //return Json(new MessageData(true, "修改成功"));
        //    }
        //    catch (Exception)
        //    {
        //        //return Json(new MessageData(false, "修改失败"));
        //        return RedirectToAction("UserAdmin");
        //    }
        //}

        //public ActionResult UserDelete(int id, UserModel model)
        //{
        //    var deluser = db.t_User.FirstOrDefault(n => n.UserId == id);
        //    deluser.IsDelete = 1;
        //    db.SaveChanges();
        //    //return Json(new MessageData(true, "删除成功"));
        //    return RedirectToAction("UserAdmin");
        //}
    }
}