﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;
using System.Linq.Expressions;

namespace Letter.Controllers
{
    public class AutoController : BaseController
    {
        [HttpPost]
        public ActionResult NoTransactStatus(string appealNumber)
        {
            var caseInfo = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == appealNumber || r.AppealTitle == appealNumber);
            if (caseInfo == null)
            {
                return Json(new MessageData(false, ""));
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2") || (CurrentUser.UserRoleList.Contains("4") && caseInfo.UnitType == CurrentUser.UserUnit))
                {
                   return Json(new MessageData(false, ""));
                }
                else
                {
                    return Json(new MessageData(true, "您对此案件没有操作权限"));
                }
            }

        }
        [HttpPost]
        public ActionResult TransactStatus(string appealNumber)
        {
            var caseInfo = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == appealNumber || r.AppealTitle == appealNumber);
            if (caseInfo == null)
            {
                return Json(new MessageData(false, ""));
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2") || (CurrentUser.UserRoleList.Contains("4") && caseInfo.UnitType == CurrentUser.UserUnit))
                {
                    if (caseInfo.IsStopPetition == 0)
                    {
                        return Json(new MessageData(true, "此案件已息访，不可添加办理记录"));
                    }
                    else
                    {
                        return Json(new MessageData(false, ""));
                    }
                }
                else
                {
                    return Json(new MessageData(true, "您对此案件没有操作权限"));
                }
            }

        }

        [HttpPost] //自动完成案件编号案件标题
        public ActionResult GetNumTitle(string appealNumber)
        {
            var userArea = CurrentUser.UserArea.ToString();
            var userUnit = CurrentUser.UserUnit.ToString();
            Expression<Func<t_CaseInfo, bool>> expressionTitle = null;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expressionTitle = (n) => n.AppealTitle.Contains(appealNumber) && n.IsDelete == 0;
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区 & 单位
                {
                    expressionTitle = (n) => n.AppealTitle.Contains(appealNumber) && n.IsDelete == 0 && (n.RelateAreaID.Contains(userArea) || n.RelateUnitID.Contains(userUnit));
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    expressionTitle = (n) => n.AppealTitle.Contains(appealNumber) && n.IsDelete == 0 && n.RelateUnitID.Contains(userUnit);
                }
                else
                {
                    expressionTitle = (n) => n.AppealTitle.Contains(appealNumber) && n.IsDelete == 0 && n.RelateAreaID.Contains(userArea);
                }

            }

            var autoTitle = db.t_CaseInfo.Where(expressionTitle);
            var resultTitle = autoTitle.Select(r => r.AppealTitle).Take(10).ToArray();

            var jsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(resultTitle);
            return Json(new MessageData(true, jsonStr));
        }  
    }
}