﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class BergabeController : BaseController
    {
        public ActionResult BergabeList()
        {
            return View();
        }
        public ActionResult BergabeRecord(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                ViewData["ID"] = id;
                return View();
            }
        }

        /// <summary>
        /// 获取案件
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCaseInfo(int pageindex, int pagesize)
        {
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = "";
            }
            else {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区 & 单位
                {
                    expression = " and (TownshipType = " + CurrentUser.UserArea + " and UnitType = " + CurrentUser.UserUnit + ")";
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    expression = " and UnitType =" + CurrentUser.UserUnit;
                }
                else
                {
                    //片区
                    expression = " and TownshipType = " + CurrentUser.UserArea;
                }

            }

            string sql = "SELECT c.ID,c.[PetitionIDNo],c.[PetitionAddress],c.[TownshipType],c.[AppealNumber],c.[AppealTitle],c.[AppealContent],c.[PetitionName] ,c.[PetitionTel],c.[IsLawLitigation],c.[IsRevisit],c.[IsReconcile],c.[IsStopPetition],co.[CountyName],r.[UnitName] FROM[LetterInfo].[dbo].[t_CaseInfo] c left join [dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId where IsDelete = 0" + expression;
            var temp = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("BergabePartial", temp);
        }

        /// <summary>
        /// 查询案件
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <param name="petitionnum"></param>
        /// <param name="appealtitle"></param>
        /// <returns></returns>
        [HttpPost] 
        public ActionResult QueryCaseInfoList(int pageindex, int pagesize, string petitionname)
        {
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = "";
            }
            else {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区 & 单位
                {
                    expression = " and (TownshipType = " + CurrentUser.UserArea + " and UnitType = " + CurrentUser.UserUnit + ")";
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    expression = " and UnitType =" + CurrentUser.UserUnit;
                }
                else
                {
                    //片区
                    expression = " and TownshipType = " + CurrentUser.UserArea;
                }

            }
            string where = string.Empty;
           
            if (petitionname.Trim() != "")
            {
                where += " and PetitionName like '%" + petitionname + "%'";
            }
           
            string sql = "SELECT c.ID,c.[PetitionIDNo],c.[PetitionAddress],c.[TownshipType],c.[AppealNumber],c.[AppealTitle],c.[AppealContent],c.[PetitionName] ,c.[PetitionTel],c.[IsLawLitigation],c.[IsRevisit],c.[IsReconcile],c.[IsStopPetition],co.[CountyName],r.[UnitName] FROM[LetterInfo].[dbo].[t_CaseInfo] c join[dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId where IsDelete = 0 " + expression + where;
            var queryList = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("BergabePartial", queryList);
        }

        /// <summary>
        /// 获取办理记录
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <param name="caseinfoid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetBergabe(int pageindex, int pagesize, int caseinfoid)
        {
            string sql = "select b.ID,b.[CaseInfoID],c.[AppealNumber],c.[TownshipType],b.[BergabeType],b.[BergabeDate],b.[TransactDetails],b.[SurveyDetails],b.[DisposeDetails],b.[FileName] from [dbo].[t_Bergabe] b inner join [dbo].[t_CaseInfo] c on b.CaseInfoID = c.ID where b.IsDelete = 0 and b.CaseInfoID =" + caseinfoid;
            var temp = db.Database.SqlQuery<BergabeModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<BergabeModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("BergabeRecordPartial", temp);
        }

        /// <summary>
        /// 搜索办理记录
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <param name="caseinfoid"></param>
        /// <param name="bergabetype"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult QueryBergabe(int pageindex, int pagesize, int caseinfoid,int bergabetype)
        {
          string sql = "select b.ID,b.[CaseInfoID],AppealNumber,b.[BergabeType],b.[BergabeDate],b.[TransactDetails],b.[SurveyDetails],b.[DisposeDetails],b.[FileName] from [dbo].[t_Bergabe] b inner join [dbo].[t_CaseInfo] c on b.CaseInfoID = c.ID where b.IsDelete = 0 and b.CaseInfoID =" + caseinfoid + " and b.[BergabeType] =" +bergabetype;
            var temp = db.Database.SqlQuery<BergabeModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<BergabeModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("BergabeRecordPartial", temp);
        }

        public ActionResult BergabeAdd()
        {
            return View();
        }

        /// <summary>
        /// 添加记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BergabeAdd(BergabeModel model)
        {
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            //var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber).ID;
            t_Bergabe tbBergabe = new t_Bergabe();
            tbBergabe.CaseInfoID = caseInfoId;
            tbBergabe.BergabeType = model.BergabeType;
            tbBergabe.BergabeDate = model.BergabeDate;
            tbBergabe.TransactDetails = model.TransactDetails;
            tbBergabe.SurveyDetails = model.SurveyDetails;
            tbBergabe.DisposeDetails = model.DisposeDetails;
            tbBergabe.FileName = model.FileName;
            tbBergabe.IsDelete = 0;
            db.t_Bergabe.Add(tbBergabe);

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception ex)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        /// <summary>
        /// 修改记录信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost] 
        public ActionResult BergabeEdit(BergabeModel model)
        {
           // var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber).ID;
            var bergabeedit = db.t_Bergabe.FirstOrDefault(n => n.ID == model.ID);

          //  bergabeedit.CaseInfoID = caseInfoId;
            bergabeedit.BergabeType = model.BergabeType;
            bergabeedit.BergabeDate = model.BergabeDate;
            bergabeedit.TransactDetails = model.TransactDetails.Trim();
            bergabeedit.SurveyDetails = model.SurveyDetails.Trim();
            bergabeedit.DisposeDetails = model.DisposeDetails;
            if(model.FileName != null)
            {
                bergabeedit.FileName = model.FileName;
            }
    
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        /// <summary>
        /// 删除记录信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost] 
        public ActionResult BergabeDel(int id)
        {
            var delBergabe = db.t_Bergabe.FirstOrDefault(n=>n.ID == id);
            delBergabe.IsDelete = 1;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }
    }
}