﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class LeaderReservationController : BaseController
    {
        public ActionResult LeaderReservation()
        {
            //var selectLeader = db.Database.SqlQuery<LeaderMeetingModel>("select LeaderId,LeaderName+'('+LeaderPosition+')' as NamePosition,Status from t_Leader").Where(n => n.Status == 0).ToList();
            //ViewData["selectLeader"] = new SelectList(selectLeader, "LeaderId", "NamePosition");

            //var selectNumber = db.t_CaseInfo.Where(n => n.IsDelete == 0).ToList();
            //ViewData["selectNumber"] = new SelectList(selectNumber, "ID", "AppealNumber");
            return View();
        }
        public ActionResult LeaderReservationList(string name)
        {
            if (name == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                var selectLeader = db.Database.SqlQuery<LeaderMeetingModel>("select LeaderId,LeaderName+'('+LeaderPosition+')' as NamePosition,Status from t_Leader").Where(n => n.Status == 0).ToList();
                ViewData["selectLeader"] = new SelectList(selectLeader, "LeaderId", "NamePosition");

                var selectNumber = db.t_CaseInfo.Where(n => n.IsDelete == 0).ToList();
                ViewData["selectNumber"] = new SelectList(selectNumber, "ID", "AppealNumber");
                ViewData["ID"] = name;
                return View();
            }
        }
        //#region
        //[HttpPost] //获取领导接访信息
        //public ActionResult GetMeeting(int pageindex, int pagesize)
        //{
        //    var temp = db.Database.SqlQuery<LeaderMeetingModel>("select s.LeaderId,LeaderName,LeaderPosition,count(distinct CaseInfoID) as Lcount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID where IsDelete = 0 Group by s.LeaderId,s.LeaderName,s.LeaderPosition")
        //        .OrderByDescending(n => n.LeaderName)
        //        .Skip(pagesize * (pageindex - 1))
        //        .Take(pagesize).ToList();

        //    ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>("select s.LeaderId,LeaderName,LeaderPosition,count(distinct CaseInfoID) as Lcount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID where IsDelete = 0 Group by s.LeaderId,s.LeaderName,s.LeaderPosition").Count();
        //    ViewData["pageindex"] = pageindex;
        //    ViewData["pagesize"] = pagesize;

        //    int pageIndex = pageindex;//当前页
        //    int pageSize = pagesize;//显示条数

        //    return PartialView("LeaderMeetingPartial", temp);
        //}

        //[HttpPost] //查询领导接访信息
        //public ActionResult QueryMeeting(int leaderId)
        //{
        //    var queryList = db.Database.SqlQuery<LeaderMeetingModel>("select s.LeaderId,LeaderName,LeaderPosition,count(m.LeaderID) as Lcount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID  where s.LeaderId = " + leaderId + " Group by s.LeaderId,s.LeaderName,s.LeaderPosition").Where(n => n.IsDelete == 0).ToList();

        //    ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>("select s.LeaderId,LeaderName,LeaderPosition,count(m.LeaderID) as Lcount from [t_LeaderMeeting] m inner join t_Leader s on s.LeaderId = m.LeaderID  where s.LeaderId = " + leaderId + " Group by s.LeaderId,s.LeaderName,s.LeaderPosition").Where(n => n.IsDelete == 0).Count();

        //    return PartialView("LeaderMeetingPartial", queryList);
        //}

        //[HttpPost] //获取诉求详情
        //public ActionResult GetContent(string appealNumber)
        //{
        //    var selectNumber = db.t_CaseInfo.FirstOrDefault(n => n.AppealNumber == appealNumber);
        //    string title = selectNumber.AppealTitle;
        //    string content = selectNumber.AppealContent;
        //    string jsonStr = string.Empty;
        //    jsonStr += "{\"title\":"+"\""+title+"\""+",\"content\":" + "\"" + content + "\"" + "}";
        //    return Json(new MessageData(true, jsonStr));
        //}

        //[HttpPost] //获取案件详情
        //public ActionResult GetCaseInfoList(int leaderId)
        //{
        //    var caseInfoList = db.Database.SqlQuery<LeaderMeetingModel>("select distinct CaseInfoID,m.LeaderID,AppealNumber,AppealTitle,AppealContent,CountyName,UnitName from [t_LeaderMeeting] m inner join t_CaseInfo s on s.ID = m.CaseInfoID inner join t_County c on c.CountyId = s.TownshipType inner join t_Responsibility r on r.TableId = s.UnitType where LeaderID = "+ leaderId + "").ToList(); //&& n.Status == 0  m.IsDelete = 0 and 

        //    return PartialView("SeeMeetingModal", caseInfoList);
        //}

        //[HttpPost] //获取领导详情
        //public ActionResult GetLeaderDetails(int leaderId)
        //{
        //    var leaderDetails = db.t_Leader.Where(n => n.LeaderId == leaderId).ToList(); //&& n.Status == 0

        //    return PartialView("SeeLeaderModal", leaderDetails);
        //}
        //#endregion
        //public ActionResult MeetingList(int id)
        //{
        //    var selectLeader = db.Database.SqlQuery<LeaderMeetingModel>("select LeaderId,LeaderName +' ' +'('+LeaderPosition+')' as NamePosition,Status from t_Leader").Where(n => n.Status == 0).ToList();
        //    ViewData["selectLeader"] = new SelectList(selectLeader, "LeaderId", "NamePosition");
        //    ViewData["ID"] = id;

        //    var selectNumber = db.t_CaseInfo.Where(n => n.IsDelete == 0).ToList();
        //    ViewData["selectNumber"] = new SelectList(selectNumber, "ID", "AppealNumber");
        //    return View();
        //}

        [HttpPost] //获取预约记录信息
        public ActionResult GetLeaderReservation(int pageindex, int pagesize, int id)
        {
            string expression = Power();
            string sql = "SELECT c.PetitionName, COUNT(c.PetitionName) as ID FROM [LetterInfo].[dbo].[t_LeaderReservation] l join [dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID where l.IsDelete = 0" + expression +"group by c.PetitionName";
            var temp = db.Database.SqlQuery<LeaderReservationModel>(sql)
               .OrderByDescending(n => n.ReceptionDateBefore)
               .Skip(pagesize * (pageindex - 1))
               .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderReservationModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderReservationPartialOuter", temp);
        }

        [HttpPost] //查询接访记录信息
        public ActionResult QueryMeetingR(int pageindex, int pagesize, string petitionname)
        {
            string expression = Power();
            //tring selectexpression = string.Empty;
           
           string sql = "SELECT c.PetitionName, COUNT(c.PetitionName) as ID FROM [LetterInfo].[dbo].[t_LeaderReservation] l join [dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID where l.IsDelete = 0 and c.PetitionName like '%" + petitionname + "%' group by c.PetitionName"  + expression;

            var queryList = db.Database.SqlQuery<LeaderReservationModel>(sql)
                .OrderByDescending(n => n.ID)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderReservationModel>(sql).Count();

            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderReservationPartialOuter", queryList);
        }


        [HttpPost] //获取预约记录信息
        public ActionResult GetLeaderReservationList(int pageindex, int pagesize, string name)
        {
            string expression = Power();
            string sql = "SELECT c.AppealNumber,l.LeaderName,l.LeaderPosition,c.AppealTitle,c.PetitionName,l.ID,l.LeaderID,l.ReceptionComment,l.ReceptionDateBefore,l.ReceptionAddressBefore,l.ReceptionPersonBefore FROM [LetterInfo].[dbo].[t_LeaderReservation] l join [dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID where l.IsDelete = 0 and c.PetitionName = '"+name +"'"+ expression;
            var temp = db.Database.SqlQuery<LeaderReservationModel>(sql)
               .OrderByDescending(n => n.ID)
               .Skip(pagesize * (pageindex - 1))
               .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderReservationModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderReservationPartial", temp);
        }


        [HttpPost] //查询接访记录信息
        public ActionResult QueryMeetingList(int pageindex, int pagesize, string selectLeader ,string name)
        {
            string expression = Power();
            //tring selectexpression = string.Empty;
            
            if (selectLeader.Length != 0)
            {
                expression += " and l.LeaderName like '%" + selectLeader.Trim()+"%'";
            }

            string sql = "SELECT c.AppealNumber,l.LeaderName,l.LeaderPosition,c.AppealTitle,c.PetitionName,l.ID,l.LeaderID,l.ReceptionComment,l.ReceptionDateBefore,l.ReceptionAddressBefore,l.ReceptionPersonBefore FROM [LetterInfo].[dbo].[t_LeaderReservation] l join [dbo].[t_CaseInfo] c on l.CaseInfoID = c.ID where l.IsDelete = 0 and c.PetitionName = '" + name + "'" + expression;

            var queryList = db.Database.SqlQuery<LeaderReservationModel>(sql)
                .OrderByDescending(n => n.ID)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<LeaderReservationModel>(sql).Count();

            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("LeaderReservationPartial", queryList);
        }

        [HttpPost] //添加预约记录
        public ActionResult AddLeaderReservation(LeaderReservationModel model)
        {
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            //var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber).ID;
            t_LeaderReservation tbReservation = new t_LeaderReservation();
            tbReservation.CaseInfoID = caseInfoId;
            tbReservation.LeaderID = null;
            tbReservation.LeaderName = model.LeaderName;
            tbReservation.LeaderPosition = model.LeaderPosition;
            tbReservation.ReceptionDateBefore = model.ReceptionDateBefore;
            tbReservation.ReceptionAddressBefore = model.ReceptionAddressBefore;
            tbReservation.ReceptionPersonBefore = model.ReceptionPersonBefore;
            tbReservation.ReceptionComment = model.ReceptionComment;
            tbReservation.IsDelete = 0;//是否删除
            //tbReservation.ReceptionStatus = 0;//预约中
            
            db.t_LeaderReservation.Add(tbReservation);
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost] //编辑预约记录
        public ActionResult EidtLeaderReservation(LeaderReservationModel model,int id)
         {
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            //var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber).ID;
            var reservationEdit = db.t_LeaderReservation.FirstOrDefault(n => n.ID == id);
            reservationEdit.CaseInfoID = caseInfoId;
            reservationEdit.LeaderID = null;
            reservationEdit.LeaderName = model.LeaderName;
            reservationEdit.LeaderPosition = model.LeaderPosition;
            reservationEdit.ReceptionDateBefore = model.ReceptionDateBefore;
            reservationEdit.ReceptionAddressBefore = model.ReceptionAddressBefore;
            reservationEdit.ReceptionPersonBefore = model.ReceptionPersonBefore;
            reservationEdit.ReceptionComment = model.ReceptionComment;
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost] //是否完成预约记录
        public ActionResult IsFinishLeaderReservation(LeaderReservationModel model, int id)
        {
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            //var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber).ID;
            var reservationEdit = db.t_LeaderReservation.FirstOrDefault(n => n.ID == id);
            reservationEdit.ReceptionDateAfter = model.ReceptionDateAfter;
            reservationEdit.ReceptionAddressAfter = model.ReceptionAddressAfter;
            reservationEdit.ReceptionPersonAfter = model.ReceptionPersonAfter;
            reservationEdit.ReceptionComment = model.ReceptionComment;
            reservationEdit.ReceptionStatus = 1;
            try
            {
                db.SaveChanges();
                
                //完成预约添加一条接访记录
                
                t_LeaderMeeting newMeeting = new t_LeaderMeeting();
                newMeeting.CaseInfoID = caseInfoId;
                newMeeting.LeaderID = model.LeaderID;
                newMeeting.ReceptionDate = model.ReceptionDateAfter;
                newMeeting.ReceptionComment = model.ReceptionComment;
                newMeeting.ReceptionAddress = model.ReceptionAddressAfter;
                newMeeting.ReceptionPerson = model.ReceptionPersonAfter;
                newMeeting.IsDelete = 0;
                db.t_LeaderMeeting.Add(newMeeting);
                db.SaveChanges();
                return Json(new MessageData(true, "完成成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "操作失败"));
            }
        }

        [HttpPost] //删除预约记录
        public ActionResult CancelLeaderReservation(int id)
        {
            var reservationEdit = db.t_LeaderReservation.FirstOrDefault(n => n.ID == id);
            reservationEdit.IsDelete = 1;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "操作失败"));
            }
        }
    }
}