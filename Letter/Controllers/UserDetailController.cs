﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class UserDetailController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult UserDetailInfo(int id)
        {
            var areaList = db.t_County.Where(n => n.Status == 0).ToList();
            ViewData["userAreaList"] = new SelectList(areaList, "CountyId", "CountyName");

            var unitList = db.t_Responsibility.Where(n => n.Status == 0).ToList();
            ViewData["userUnitList"] = new SelectList(unitList, "TableId", "UnitName");

            var useredit = db.t_User.FirstOrDefault(n => n.UserId == id);
            UserModel tbuser = new UserModel();
            tbuser.UserId = useredit.UserId;
            tbuser.LoginName = useredit.LoginName.Trim();
            tbuser.UserName = useredit.UserName.Trim();
            tbuser.UserPosition = useredit.UserPosition;
            tbuser.UserPhone = useredit.UserPhone;
            tbuser.UserIDNumber = useredit.UserIDNumber;
            tbuser.UserPassword = useredit.UserPassword;
            
            return PartialView("UserDetailPartial", tbuser);
        }

        [HttpPost]
        public ActionResult UserDetailInfoEdit(UserModel model)
        {
            var useredit = db.t_User.FirstOrDefault(n => n.UserId == model.UserId);
            useredit.UserName = model.UserName.Trim();
            useredit.UserPassword = model.UserPassword.Trim();
            useredit.UserPosition = model.UserPosition;
            useredit.UserPhone = model.UserPhone;
            useredit.UserIDNumber = model.UserIDNumber;
            //useredit.UserArea = model.UserArea;
            //useredit.UserUnit = model.UserUnit;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost]
        public ActionResult GetMessage()
        {
            //string expression = Power();
            string expression = string.Empty;
            if (CurrentUser.UserRoleList.Contains("1") || CurrentUser.UserRoleList.Contains("2"))//admin
            {
                expression = "";
            }
            else
            {
                if (CurrentUser.UserRoleList.Contains("3") && CurrentUser.UserRoleList.Contains("4"))//片区
                {
                    expression = " and (RelateUnitID like '%" + CurrentUser.UserUnit + "%' and RelateAreaID like '%" + CurrentUser.UserArea + "%')";
                }
                else if (CurrentUser.UserRoleList.Contains("4"))//单位
                {
                    expression = " and RelateUnitID  like '%" + CurrentUser.UserUnit + "%'";
                }

                else //片区
                {
                    expression = " and RelateAreaID like '%" + CurrentUser.UserArea + "%'";
                }

            }
            string sql = @"select top 3 * from [dbo].[t_CaseInfo] c left join [dbo].[t_PackageCasePersonnel] p on c.ID= p.CaseInfoID 
                          where IsDelete = 0  and ([PrefectureLeaderName] is null or [OfficeLeaderName] is null or [DirectLeaderName] is null or
                           [StabilityLeaderName] is null or [BasicLeaderName] is null or [BasicPoliceName] is null or [OfficeDirectLeaderName] is null  ) " + expression + " order by c.[ID] desc";
            var temp = db.Database.SqlQuery<PackageCaseInfoModel>(sql).ToList();
            string sqltotal = @"select * from [dbo].[t_CaseInfo] c left join [dbo].[t_PackageCasePersonnel] p on c.ID= p.CaseInfoID 
                          where IsDelete = 0  and ([PrefectureLeaderName] is null or [OfficeLeaderName] is null or [DirectLeaderName] is null or
                          [StabilityLeaderName] is null or [BasicLeaderName] is null or [BasicPoliceName] is null or [OfficeDirectLeaderName] is null ) " + expression;
            ViewData["total"] = db.Database.SqlQuery<PackageCaseInfoModel>(sqltotal).Count();
            return PartialView("MessagePartial", temp);
        }
    }
}