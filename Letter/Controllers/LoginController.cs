﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class LoginController : BaseController
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost] //登陆
        public ActionResult Login(UserModel model)
        {
            if(model.LoginName == null)
            {
                ViewData["LoginErr"] = "请输入用户名";
                return View();
            }
            else if(model.UserPassword == null)
            {
                ViewData["LoginErr"] = "请输入密码";
                return View();
            }
            else
            {
                var existsModel = db.t_User.FirstOrDefault(u => u.LoginName == model.LoginName && u.UserPassword == model.UserPassword && u.IsDelete == 0);
                if (existsModel == null)
                {
                    ViewData["LoginErr"] = "用户名或密码错误";
                    return View();
                }
                else
                {
                    var user = db.t_User.Where(u => u.LoginName == model.LoginName).FirstOrDefault();
                    Session["UserArea"] = user.UserArea;
                    Session["UserUnit"] = user.UserUnit;
                    Session["CurrentUser"] = user;
                    return RedirectToAction("PackageCaseList", "PackageCase");
                }
            }
        }
    }
}