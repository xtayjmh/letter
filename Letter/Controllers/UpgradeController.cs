﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Letter.Controllers
{
    public class UpgradeController : BaseController
    {
        public ActionResult Upgrade()
        {
            return View();
        }
        public ActionResult UpgradeList(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                ViewData["ID"] = id;
                return View();
            }
        }
        [HttpPost] //获取案件信息
        public ActionResult GetCaseInfo(int pageindex, int pagesize)
        {
            string sql = "SELECT C.ID as ID, u.CaseInfoID,c.[PetitionName],co.[CountyName],r.[UnitName], COUNT(u.CaseInfoID) as CountID FROM[LetterInfo].[dbo].[t_CaseInfo] c left join [dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId inner join [dbo].[t_Record] u on c.ID = u.CaseInfoID where c.IsDelete = 0 and u.IsDelete = 0 Group by  u.CaseInfoID,c.[PetitionName],co.[CountyName],r.[UnitName],C.ID";
            var temp = db.Database.SqlQuery<t_UpgradeList>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<t_UpgradeList>(sql).Count();

            return PartialView("UpgradePartial", temp);
        }
        [HttpPost] //查询信息 //int pageindex, int pagesize, string petitionName,string unitName,string countyName, string recordType, string startDate, string endDate
        public ActionResult QueryCaseInfo(string jsonStr)
        {
            JObject json = (JObject)JsonConvert.DeserializeObject(jsonStr);
            int pageindex = Convert.ToInt32(json["pageindex"].ToString());
            int pagesize = Convert.ToInt32(json["pagesize"].ToString());
            string petitionName = json["petitionName"].ToString();
            string unitName = json["unitName"].ToString();
            string countyName = json["countyName"].ToString();

            string where = string.Empty;

            if (petitionName.Trim() != "")
            {
                where += " and PetitionName like '%" + petitionName + "%'";
            }
            if (unitName.Trim() != "")
            {
                where += " and UnitName like '%" + unitName + "%'";
            }
            if (countyName.Trim() != "")
            {
                where += " and CountyName like '%" + countyName + "%'";
            }

            string sql = "SELECT u.CaseInfoID,c.[PetitionName],co.[CountyName],r.[UnitName], COUNT(u.CaseInfoID) as ID FROM[LetterInfo].[dbo].[t_CaseInfo] c left join [dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId inner join [dbo].[t_Record] u on c.ID = u.CaseInfoID where c.IsDelete = 0 and u.IsDelete = 0 "+ where + " Group by  u.CaseInfoID,c.[PetitionName],co.[CountyName],r.[UnitName]";
            var queryList = db.Database.SqlQuery<t_UpgradeList>(sql)
                            .OrderByDescending(n => n.RecordDate)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<t_UpgradeList>(sql).Count();

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UpgradePartial", queryList);
        }
        [HttpPost] //获取案件信息
        public ActionResult GetCaseInfoList(int pageindex, int pagesize, int id)
        {
            string sql = "SELECT u.ID,u.CaseInfoID,c.[AppealTitle],c.[PetitionName],co.[CountyName],r.[UnitName],u.[RecordType],u.[RecordDate],u.[RecordFileName],u.[RecordContent] FROM[LetterInfo].[dbo].[t_CaseInfo] c left join [dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId inner join [dbo].[t_Record] u on c.ID = u.CaseInfoID where c.IsDelete = 0 and u.IsDelete = 0 and u.CaseInfoID = " + id;
            var temp = db.Database.SqlQuery<t_UpgradeList>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<t_UpgradeList>(sql).Count();

            return PartialView("UpgradeListPartial", temp);
        }

        [HttpPost] //查询信息 //int pageindex, int pagesize, string petitionName,string unitName,string countyName, string recordType, string startDate, string endDate
        public ActionResult QueryCaseInfoList(string jsonStr)
        {
            JObject json = (JObject)JsonConvert.DeserializeObject(jsonStr);
            int id = Convert.ToInt32(json["id"].ToString());
            int pageindex = Convert.ToInt32(json["pageindex"].ToString());
            int pagesize = Convert.ToInt32(json["pagesize"].ToString());
            //string petitionName = json["petitionName"].ToString();
            //string unitName = json["unitName"].ToString();
            //string countyName = json["countyName"].ToString();
            string recordType = json["recordType"].ToString();
            string startDate = json["startDate"].ToString();
            string endDate = json["endDate"].ToString();

            string where = string.Empty;

            //if (petitionName.Trim() != "")
            //{
            //    where += " and PetitionName like '%" + petitionName + "%'";
            //}
            //if (unitName.Trim() != "")
            //{
            //    where += " and UnitName like '%" + unitName + "%'";
            //}
            //if (countyName.Trim() != "")
            //{
            //    where += " and CountyName like '%" + countyName + "%'";
            //}
            if (recordType != "99")
            {
                where += " and RecordType = " + recordType;
            }
            if (startDate != "")
            {
                where += " and RecordDate >= '" + startDate + "'";
            }
            if (endDate.Length != 0)
            {
                where += " and RecordDate <= '" + endDate + "'";
            }

            string sql = "SELECT u.ID,u.CaseInfoID,c.[AppealTitle],c.[PetitionName],co.[CountyName],r.[UnitName],u.[RecordType],u.[RecordDate],u.[RecordFileName],u.[RecordContent] FROM[LetterInfo].[dbo].[t_CaseInfo] c left join [dbo].[t_County] co on c.TownshipType = co.CountyId left join [dbo].[t_Responsibility] r on c.UnitType = r.TableId inner join [dbo].[t_Record] u on c.ID = u.CaseInfoID where c.IsDelete = 0 and u.IsDelete = 0 and u.CaseInfoID = "+ id + where;
            var queryList = db.Database.SqlQuery<t_UpgradeList>(sql)
                            .OrderByDescending(n => n.RecordDate)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<t_UpgradeList>(sql).Count();

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UpgradeListPartial", queryList);
        }

        [HttpPost] //获进京赴省记录
        public ActionResult GetUpgradeList(int pageindex, int pagesize, int id)
        {
            var temp = db.t_Record.Where(n => n.CaseInfoID == id)
                .OrderByDescending(n => n.ID)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();

            ViewData["total"] = db.t_Record.Where(n => n.CaseInfoID == id).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UpgradeDetailsPartial", temp);
        }

        [HttpPost] //查询信息
        public ActionResult QueryUpgradeList(int pageindex, int pagesize, string number, string title, int id, string recordType)
        {
            var queryList = db.t_Record.Where(n => n.t_CaseInfo.AppealNumber.Contains(number) && n.t_CaseInfo.AppealTitle.Contains(title) && n.t_CaseInfo.IsDelete == 0 && n.RecordType.Contains(recordType) && n.CaseInfoID == id)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_Record.Where(n => n.t_CaseInfo.AppealNumber.Contains(number) && n.t_CaseInfo.AppealTitle.Contains(title) && n.t_CaseInfo.IsDelete == 0 && n.RecordType.Contains(recordType) && n.CaseInfoID == id).Count();

            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UpgradeDetailsPartial", queryList);
        }


        public ActionResult UpgradeDetails(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                ViewData["ID"] = id;
                return View();
            }
        }
        [HttpPost]
        public ActionResult AddUpgrader(UpgradeDetailModel model)
        {
            var caseInfoId1 = db.t_CaseInfo.Where(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ToList();
            var caseInfoId = db.t_CaseInfo.FirstOrDefault(r => r.AppealNumber == model.AppealNumber || r.AppealTitle == model.AppealNumber).ID;
            t_Record newMeeting = new t_Record();
            newMeeting.CaseInfoID = caseInfoId;
            newMeeting.RecordType = model.RecordType.Trim();
            newMeeting.RecordDate = model.RecordDate;
            newMeeting.RecordContent = model.RecordContent;
            if (model.RecordFileName == null)
                newMeeting.RecordFileName = model.RecordFileName;
            else
                newMeeting.RecordFileName = model.RecordFileName.Trim();

            db.t_Record.Add(newMeeting);
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception ex)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost]
        public ActionResult EditUpgrader(UpgradeDetailModel model)
        {
            var newMeeting = db.t_Record.FirstOrDefault(n => n.ID == model.ID);
            newMeeting.RecordType = model.RecordType;
            newMeeting.RecordDate = model.RecordDate;
            newMeeting.RecordContent = model.RecordContent;
            if (model.RecordFileName != null)
                newMeeting.RecordFileName = model.RecordFileName.Trim();
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception ex)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }
        [HttpPost]
        public ActionResult DelUpgrader(int id)
        {
            var newMeeting = db.t_Record.FirstOrDefault(n => n.ID == id);
            newMeeting.IsDelete = 1;
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception ex)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }
    }
}