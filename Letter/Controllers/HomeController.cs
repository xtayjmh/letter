﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;

namespace Letter.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("PackageCaseList", "PackageCase");
        }

        /// <summary>
        /// 党政领导接访
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetLeaderMeeting()
        {
            string sql = "SELECT TOP 10 [ID],[CaseInfoID],[LeaderID],[ReceptionDate],[ReceptionComment],[IsDelete],[ReceptionAddress],[ReceptionPerson] FROM [LetterInfo].[dbo].[t_LeaderMeeting] where IsDelete = 0 order by ID desc";
            var temp = db.Database.SqlQuery<LeaderMeetingModel>(sql).ToList();
            ViewData["total"] = db.Database.SqlQuery<LeaderMeetingModel>(sql).Count();

            return PartialView("HomeLeaderMeeting", temp);
        }

        /// <summary>
        /// 获取党政领导预约记录信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetLeaderReservation()
        {
            string sql = "SELECT TOP 10 [ID],[CaseInfoID],[LeaderID],[ReceptionDateBefore],[ReceptionComment],[IsDelete],[ReceptionAddressBefore],[ReceptionPersonBefore],[ReceptionStatus],[ReceptionDateAfter],[ReceptionAddressAfter],[ReceptionPersonAfter] FROM [LetterInfo].[dbo].[t_LeaderReservation] where IsDelete = 0 order by ID desc";
            var temp = db.Database.SqlQuery<LeaderReservationModel>(sql).ToList();
            ViewData["total"] = db.Database.SqlQuery<LeaderReservationModel>(sql).Count();

            return PartialView("HomeLeaderReservationPartial", temp);
        }

        /// <summary>
        /// 获取案件包案信息
        /// </summary>
        /// <returns></returns>
        [HttpPost] 
        public ActionResult GetPackageCase()
        {
            string sql = "select top 10 * from [dbo].[t_PackageCasePersonnel] where PrefectureLeaderName is null or OfficeLeaderName is null or DirectLeaderName is null or StabilityLeaderName is null or BasicLeaderName is null or BasicPoliceName is null order by ID desc";
            var temp = db.Database.SqlQuery<PackageCaseInfoModel>(sql).ToList();
            ViewData["total"] = db.Database.SqlQuery<PackageCaseInfoModel>(sql).Count();

            return PartialView("HomePackageCasePartial", temp);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UserAdmin()
        {
            return View();
        }
        public ActionResult UserAdd()
        {
            return View();
        }

        //片区/乡镇管理
        public ActionResult RegionData(CountyModel model)
        {
            return View();
        }

        //党政领导信息管理
        public ActionResult LeaderData()
        {
            return View();
        }

        //责任单位
        public ActionResult CompanyData()
        {
            return View();
        }

        public ActionResult test()
        {
            ViewBag.Message = "Your contact page.";
            return View();

        }

        [HttpPost]
        public ActionResult GetRegion(int pageindex, int pagesize)
        {
            var list = db.t_County.ToList();
            List<t_County> newList = new List<t_County>();
            foreach (t_County item in list)
            {
                if (item.Status == 0)
                {
                    newList.Add(item);
                }
            }

            ViewData["total"] = newList.Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            //newList.ToArray;

            //获取需要展示的部门数据
            //var list = Data.FindGold.ToList()

            //得到数据的条数
            int rowCount = newList.Count();

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//这里用来设置每页要展示的数据数量，建议把这个写到web.config中来全局控制

            //通过计算，得到分页应该需要分几页，其中不满一页的数据按一页计算
            if (rowCount % pageSize != 0)
            {
                rowCount = rowCount / pageSize + 1;
            }
            else
            {
                rowCount = rowCount / pageSize;
            }

            newList = newList.Skip(pageIndex * pageSize - pagesize).Take(pageSize).ToList();
            return PartialView("RegionPartial", newList);
        }

        [HttpPost]
        public ActionResult AddRegion(CountyModel model)
        {
            t_County newCounty = new t_County();
            newCounty.CountyName = model.CountyName;

            db.t_County.Add(newCounty);

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost]
        public ActionResult EditRegion(CountyModel model)
        {
            var newCounty = db.t_County.FirstOrDefault(n => n.CountyId == model.ID);
            newCounty.CountyName = model.CountyName;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }


        }

        [HttpPost]
        public ActionResult DelRegion(int id)
        {
            var newCounty = db.t_County.FirstOrDefault(n => n.CountyId == id);
            newCounty.Status = 1;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }


        }

    }
}