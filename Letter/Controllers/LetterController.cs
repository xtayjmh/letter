﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using DAL;
using System.Linq.Expressions;

namespace Letter.Controllers
{
    public class LetterController : BaseController
    {
        public ActionResult LetterList()
        {
            return View();
        }
        public ActionResult Letter(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Login", "Login");
            }
            {
                ViewData["ID"] = id;
                return View();
            }
        }


        /// <summary>
        /// 获取案件信息
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCaseInfo(int pageindex, int pagesize)
        {
            string expression = Power();
            string sql = "SELECT ID,[TownshipType],[UnitType],[RelateUnitID],[RelateUnitName],[RelateAreaID],[RelateAreaName],[AppealNumber],[AppealTitle],[PetitionName] ,[PetitionTel],[IsLawLitigation],[IsRevisit],[IsReconcile],[IsStopPetition] FROM[LetterInfo].[dbo].[t_CaseInfo] where IsDelete = 0" + expression;
            var temp = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();
            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("TransactPartial", temp);
        }

        /// <summary>
        /// 查询案件信息
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <param name="number"></param>
        /// <param name="title"></param>
        /// <param name="stopPetition"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult QueryCaseInfoList(int pageindex, int pagesize, string petitionname, int stopPetition)
        {
            string expression = Power();

            string where = string.Empty;
           
            if (petitionname.Trim() != "")
            {
                where += " and PetitionName like '%" + petitionname + "%'";
            }
            
            if (stopPetition != 99)
            {
                where += " and IsStopPetition = " + stopPetition;
            }

            string sql = "SELECT ID,[TownshipType],[UnitType],[RelateUnitID],[RelateUnitName],[RelateAreaID],[RelateAreaName],[AppealNumber],[AppealTitle],[PetitionName] ,[PetitionTel],[IsLawLitigation],[IsRevisit],[IsReconcile],[IsStopPetition] FROM[LetterInfo].[dbo].[t_CaseInfo] where IsDelete = 0" + where + expression ;
            var queryList = db.Database.SqlQuery<CaseInfoModel>(sql)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.Database.SqlQuery<CaseInfoModel>(sql).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("TransactPartial", queryList);
        }

        [HttpPost]
        public ActionResult GetTransact(int pageindex, int pagesize,int caseinfoid)
        {
            var temp = db.t_Transact.Where(n => n.CaseInfoID == caseinfoid && n.TransactType != 99)
                .OrderByDescending(n => n.ID)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();
            ViewData["total"] = db.t_Transact.Where(n => n.CaseInfoID == caseinfoid).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("TransactListPartial", temp);
        }

        [HttpPost] //查询信息
        public ActionResult QueryTransactList(int pageindex, int pagesize, int caseinfoid, int transactType, int reconcile, int stopPetition)
        {
            Expression<Func<t_Transact, bool>> expression = null;
            if (transactType != 99 && reconcile == 99 && stopPetition == 99)
            {
                expression = (r) => r.TransactType == transactType && r.CaseInfoID == caseinfoid;
            }
            else if (transactType == 99 && reconcile != 99 && stopPetition == 99)
            {
                expression = (r) => r.IsReconcile == reconcile && r.CaseInfoID == caseinfoid;
            }
            else if (transactType == 99 && reconcile == 99 && stopPetition != 99)
            {
                expression = (r) => r.IsStopPetition == stopPetition && r.CaseInfoID == caseinfoid;
            }
            else if (transactType != 99 && reconcile != 99 && stopPetition == 99)
            {
                expression = (r) => r.TransactType == transactType && r.IsReconcile == reconcile && r.CaseInfoID == caseinfoid;
            }
            else if (transactType != 99 && reconcile == 99 && stopPetition != 99)
            {
                expression = (r) => r.TransactType == transactType && r.IsStopPetition == stopPetition && r.CaseInfoID == caseinfoid;
            }
            else if (transactType == 99 && reconcile != 99 && stopPetition != 99)
            {
                expression = (r) => r.IsReconcile == reconcile && r.IsStopPetition == stopPetition && r.CaseInfoID == caseinfoid;
            }
            else
            {
                expression = (r) => r.TransactType == transactType && r.IsReconcile == reconcile && r.IsStopPetition == stopPetition && r.CaseInfoID == caseinfoid;
            }


            var queryList = db.t_Transact.Where(expression)
                            .OrderByDescending(n => n.ID)
                           .Skip(pagesize * (pageindex - 1))
                           .Take(pagesize).ToList();

            ViewData["total"] = db.t_Transact.Where(expression).Count();

            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            return PartialView("TransactListPartial", queryList);
        }

       


        public ActionResult TransactAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TransactAdd(TransactModel model)
        {
            var caseInfo = db.t_CaseInfo.FirstOrDefault(r => r.ID == model.CaseInfoID);
            if (caseInfo.IsStopPetition == 0)
            {
                return Json(new MessageData(true, "此案件已息访，不可添加办理记录"));
            }
            else
            {
                t_Transact tbTransact = new t_Transact();
                tbTransact.CaseInfoID = caseInfo.ID;
                tbTransact.TransactType = model.TransactType;
                tbTransact.TransactDate = model.TransactDate;
                tbTransact.FileName = model.FileName;
                tbTransact.IsStopPetition = model.IsStopPetition;//息访
                tbTransact.StopPetitionDate = model.StopPetitionDate;
                tbTransact.StopPetitionLetter = model.StopPetitionLetter;
                tbTransact.IsReconcile = model.IsReconcile;//化解
                tbTransact.ReconcileDate = model.ReconcileDate;
                tbTransact.ReconcileContent = model.ReconcileContent;
                tbTransact.TellFile = model.TellFile;
                tbTransact.TwoWayFile = model.TwoWayFile;
                tbTransact.ReviewFile = model.ReviewFile;
                tbTransact.StatusFile = model.StatusFile;
                db.t_Transact.Add(tbTransact);
                try
                {
                    db.SaveChanges();
                    if (model.IsStopPetition == 0)
                    {
                        caseInfo.IsStopPetition = 0;
                        db.SaveChanges();
                    }
                    if (model.IsReconcile == 0)
                    {
                        caseInfo.IsReconcile = 0;
                        db.SaveChanges();
                    }
                    return Json(new MessageData(true, "添加成功"));
                }
                catch (Exception ex)
                {
                    return RedirectToAction("LetterList");
                }

               
            }
        }

        [HttpPost]
        public ActionResult TransactDel(int id)
        {
            var del = db.t_Transact.FirstOrDefault(r => r.ID == id);
            del.TransactType = 99;
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }

    }
}