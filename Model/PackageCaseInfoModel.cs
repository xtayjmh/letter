﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class PackageCaseInfoModel
    {
        public List<PackageCaseInfoModel> List { get; set; }
        public int ID { get; set; }
        public int? CaseInfoID { get; set; }//案件ID
        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//标题
        public string PetitionName { get; set; }//信访人姓名

        public int? TownshipType { get; set; }//乡镇
        public string RelateAreaID { get; set; }
        public string RelateAreaName { get; set; } //乡镇

        public int? UnitType { get; set; }//责任单位
        public string RelateUnitID { get; set; }
        public string RelateUnitName { get; set; } //责任单位

        public string PrefectureLeaderName { get; set; }//县级包案领导名字
        public string PrefectureLeaderPosition { get; set; }//县级包案领导职位
        public string OfficeLeaderName { get; set; }//责任单位包案领导名字
        public string OfficeLeaderPosition { get; set; }//责任单位包案领导职位

        public string OfficeDirectLeaderName { get; set; }//责任单位负责人名字
        public string OfficeDirectLeaderPosition { get; set; }//责任单位负责人职位

        public string DirectLeaderName { get; set; }//直接负责人名字
        public string DirectLeaderPosition { get; set; }//直接负责人职位
        public string StabilityLeaderName { get; set; }//稳控包案领导名字
        public string StabilityLeaderPosition { get; set; }//稳控包案领导职位
        public string BasicLeaderName { get; set; }//基层干部名字
        public string BasicLeaderPosition { get; set; }//基层干部职位
        public string BasicPoliceName { get; set; }//基层干警名字
        public string BasicPolicePosition { get; set; }//基层干警职位
        
    }
}