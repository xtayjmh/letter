﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public partial class t_User
    {
        public List<string> UserRoleList
        {
            get
            {
                if (!string.IsNullOrEmpty(UserRole))
                {
                    if (UserRole.Contains(","))
                    {
                        return UserRole.Split(',').ToList();
                    }
                    else
                    {
                        return new List<string>() { UserRole };
                    }
                }
                return null;
            }
        }

    }
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }//名字
        public string LoginName { get; set; }//登录名
        public string UserPassword { get; set; }//密码
        public string UserPosition { get; set; }//职务
        public string UserPhone { get; set; }//联系电话
        public string UserIDNumber { get; set; }//身份证号
        public Nullable<int> UserArea { get; set; }//区域
        public int? UserUnit { get; set; }//单位

        public string UserRole { get; set; }

        public List<string> UserRoleList
        {
            get
            {
                if (!string.IsNullOrEmpty(UserRole))
                {
                    if (UserRole.Contains(","))
                    {
                        return UserRole.Split(',').ToList();
                    }
                    else
                    {
                        return new List<string>() { UserRole };
                    }
                }
                return null;
            }
        }

        public int? IsDelete { get; set; }//是否删除

        public string CountyName { get; set; }
        public string UnitName { get; set; }
    }
}