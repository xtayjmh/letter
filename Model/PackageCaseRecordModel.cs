﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class PackageCaseRecordModel
    {
        public int ID { get; set; }
        public int? PackageCaseID { get; set; }//包案人员ID
        public string PackageCaseType { get; set; }//包案领导类型
        public string BeforeName { get; set; }//修改前名字
        public string BeforePosition { get; set; }//修改前职位
        public string AfterName { get; set; }//修改后名字
        public string AfterPosition { get; set; }//修改后职位
        public DateTime UpdateDate { get; set; }//修改时间
        public string UpdatePerson { get; set; }//修改人
        
    }
}