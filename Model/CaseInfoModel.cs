﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class CaseInfoModel
    {
        public int ID { get; set; }
        public string AppealNumber { get; set; }//案件编号
        public string PetitionName { get; set; }//信访人姓名
        public string PetitionIDNo { get; set; }//身份证号
        public int? TownshipType { get; set; }//乡镇
        public int? UnitType { get; set; }//责任单位
        public string PetitionAddress { get; set; }//地址
        public string PetitionTel { get; set; }//联系电话
        public int? IsLawLitigation { get; set; }//是否涉法涉诉
        public string strLawLitigation
        {
            get
            {
                return IsLawLitigation == 1 ? "否" : "是";
            }
        }
        public int? PetitionType { get; set; }//信访性质
        public string strPetitionType
        {
            get
            {
                return PetitionType == 1 ? "集体访" : "个人访";
            }
        }
        public int? IsRevisit { get; set; }//是否是初/重访
        public string strRevisit
        {
            get
            {
                return IsRevisit == 1 ? "重访" : "初访";
            }
        }
        public int? PeopleCount { get; set; }//人数
        public DateTime? CheckingDate { get; set; }//排查日期
        public string AppealTitle { get; set; }//标题
        public string AppealContent { get; set; }//内容
        public int? IsReceptionSite { get; set; }//是否到非接待场所涉访
        public string strReceptionSite
        {
            get
            {
                return IsReceptionSite == 1 ? "否" : "是";
            }
        }
        public string ReceptionSite { get; set; }//非接待场所涉访
        public DateTime? ReceptionSiteDate { get; set; }//非接待场所涉访时间
        public int? IsSuperior { get; set; }//是否上级交办
        public string strSuperior
        {
            get
            {
                return IsSuperior == 1 ? "否" : "是";
            }
        }
        public int? IsPackageCase { get; set; }//是否上级领导包案督访
        public string strPackageCase
        {
            get
            {
                return IsPackageCase == 1 ? "否" : "是";
            }
        }
        public string PackageCaseRemark { get; set; }//上级领导包案督访备注
        public int? IsAccumulatedCases { get; set; }//是否积案
        public string strAccumulatedCases
        {
            get
            {
                return IsAccumulatedCases == 1 ? "否" : "是";
            }
        }//是否积案
        public string AccumulatedCaseRemark { get; set; }//积案备注

        public int? IsReconcile { get; set; }//是否化解

        public DateTime? ReconcileDate { get; set; }//化解时间
        public string ReconcileContent { get; set; }//化解内容
        public int? IsStopPetition { get; set; }//是否息访
        public DateTime? StopPetitionDate { get; set; }//息访时间
        public string StopPetitionLetter { get; set; }//息访文件
        public int? IsDelete { get; set; }//是否删除


        public string RelateUnitID { get; set; }
        public string RelateUnitName { get; set; }

        public string RelateAreaID { get; set; }
        public string RelateAreaName { get; set; }

        public string CountyName { get; set; }
        public string UnitName { get; set; }
    }
}