﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class UpgradeDetailModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public string RecordType { get; set; }//类型
        public DateTime RecordDate { get; set; }//日期
        public string RecordContent { get; set; }//内容
        public string RecordFileName { get; set; }//文件名
        public string AppealNumber { get; set; }//案件编号
        //public string AppealTitle { get; set; }//案件
    }
}