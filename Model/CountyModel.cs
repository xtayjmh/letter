﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class CountyModel
    {
        public List<t_County> CountyList { get; set; }
        public int ID { get; set; } //ID
        public string CountyName { get; set; } //乡镇/片区名称
        public int CountyLevel { get; set; }
        public int ParentLevel { get; set; }
        public string CountyCode { get; set; } //区县编码
    }
}