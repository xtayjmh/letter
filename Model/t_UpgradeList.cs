﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class t_UpgradeList
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public string RecordType { get; set; }//类型
        public DateTime RecordDate { get; set; }//日期
        public string RecordContent { get; set; }//内容
        public string RecordFileName { get; set; }//文件名
        public string AppealTitle { get; set; }//案件编号
        public string PetitionName { get; set; }//信访人姓名
        public int CountyId { get; set; }
        public int TableId { get; set; }
        public string UnitName { get; set; }//责任单位
        public string CountyName { get; set; }//稳控单位
        public string UserRole { get; set; }

        public int CountID { get; set; } //ID

    }
}