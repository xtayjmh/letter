﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class LeaderMeetingModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public int LeaderID { get; set; }//领导ID
        public DateTime ReceptionDate { get; set; }//接访时间
        public string ReceptionAddress { get; set; } //接访地址
        public string ReceptionPerson { get; set; } //参与人员
        public string ReceptionComment { get; set; } //意见
        public int IsDelete { get; set; } //是否删除
        public string RelateUnitName { get; set; }
        public string RelateAreaName { get; set; }
        

        public string LeaderName { get; set; }//领导姓名
        public string LeaderPosition { get; set; }//领导职位
        public int Lcount { get; set; }
        public int Ncount { get; set; }

        public string NamePosition { get; set; }
        public int Status { get; set; }

        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//标题
        public string AppealContent { get; set; }//内容
        public string PetitionName { get; set; }//信访人姓名
        public string CountyName { get; set; }//乡镇
        public string UnitName { get; set; }//责任单位

    }
}