﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class TransactModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public int TransactType { get; set; } //办理类型
        public string FileName { get; set; } //答复意见书
        public DateTime? TransactDate { get; set; } = null;//办理日期

        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//案件标题

        public int? IsStopPetition { get; set; }//是否息访
        public DateTime? StopPetitionDate { get; set; }//息访时间
        public string StopPetitionLetter { get; set; }//息访文件

        public int? IsReconcile { get; set; }//是否化解
        public DateTime? ReconcileDate { get; set; }//化解时间
        public string ReconcileContent { get; set; }//化解内容
        public string TellFile { get; set; }//
        public string TwoWayFile { get; set; }//
        public string ReviewFile { get; set; }//
        public string StatusFile { get; set; }//
    }
}