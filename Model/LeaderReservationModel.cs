﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class LeaderReservationModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public Nullable<int> LeaderID { get; set; }//领导ID
        public DateTime? ReceptionDateBefore { get; set; } = null;//接访时间
        public string ReceptionAddressBefore { get; set; } //接访地址
        public string ReceptionPersonBefore { get; set; } //参与人员

        public DateTime? ReceptionDateAfter { get; set; } = null;//接访时间
        public string ReceptionAddressAfter { get; set; } //接访地址
        public string ReceptionPersonAfter { get; set; } //参与人员

        public string ReceptionComment { get; set; } //意见
        public int IsDelete { get; set; } //是否删除
        public int ReceptionStatus { get; set; }//状态


        public string LeaderName { get; set; }
        public string LeaderPosition { get; set; }
        public int Lcount { get; set; }

        public string NamePosition { get; set; }
        public int Status { get; set; }

        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//标题
        public string AppealContent { get; set; }//内容
        public string CountyName { get; set; }//乡镇
        public string UnitName { get; set; }//责任单位
        public string PetitionName { get; set; }//信访人姓名
    }
}