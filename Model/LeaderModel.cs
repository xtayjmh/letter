﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class LeaderModel
    {
        public int ID { get; set; } //ID
        public string LeaderName { get; set; } //领导姓名
        public string LeaderSex { get; set; } //领导性别
        public string LeaderPostion { get; set; } //领导职位
        public string LeaderPhone { get; set; }
        public string LeaderAddress { get; set; }
        public string Remark { get; set; } //备注
    }
}