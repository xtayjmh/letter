﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class BergabeModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public int BergabeType { get; set; } //转送类型
        public DateTime BergabeDate { get; set; } //日期
        public string TransactDetails { get; set; } //办理详情
        public string SurveyDetails { get; set; } //调查详情
        public string DisposeDetails { get; set; } //处理意见
        public string FileName { get; set; } //相关附件
        public int IsDelete { get; set; } //删除

        //案件信息
        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//案件标题
        public int? TownshipType { get; set; }//乡镇
        public string PetitionName { get; set; }//信访人姓名
        public string PetitionIDNo { get; set; }//身份证号
        public string PetitionAddress { get; set; }//地址
    }
}