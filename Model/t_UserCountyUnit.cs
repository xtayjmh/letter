﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class t_UserCountyUnit
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string UserPassword { get; set; }
        public string UserPosition { get; set; }
        public string UserPhone { get; set; }
        public string UserIDNumber { get; set; }
        public Nullable<int> UserArea { get; set; }
        public Nullable<int> UserUnit { get; set; }
        public Nullable<int> UserLevel { get; set; }
        public int IsDelete { get; set; }

        public int CountyId { get; set; }
        public int TableId { get; set; }

        public string CountyName { get; set; }
        public string UnitName { get; set; }
        public string UserRole { get; set; }
        
    }
}