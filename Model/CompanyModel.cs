﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class CompanyModel
    {
        public int ID { get; set; } //ID
        public string UnitName { get; set; } //乡镇/片区名称
        public string UnitCode { get; set; }
    }
}