﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{ 
    public class LeaderMeetingDetailModel
    {
        public int ID { get; set; } //ID
        public int CaseInfoID { get; set; } //案件ID
        public string AppealNumber { get; set; }//案件编号
        public string AppealTitle { get; set; }//案件标题
        public string AppealContent { get; set; }//案件内容
        public int LeaderID { get; set; }//领导ID
        public string LeaderName { get; set; } //领导姓名
        public string LeaderPostion { get; set; } //领导职位
        public DateTime ReceptionDate { get; set; }//接访时间
        public string ReceptionComment { get; set; } //意见
        public int IsDelete { get; set; } //是否删除
    }
}